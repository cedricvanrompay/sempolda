# Sempolda

https://sempolda.cedricvanrompay.fr

A database and search engine about political news. This project is in a **very early stage** for now.

There is no technical documentation for now, sorry. A first round of documentation will be done in [this issue](https://gitlab.com/cedricvanrompay/sempolda/-/issues/66). Also the source code seriously needs some grooming, but for now I am focusing on features.

In a nutshell, the project is centered around a database of web documents related to politics (named *“poldocs”* in the technical jargon of the project), and links them to topics. Then when you enter a topic in the search engine it lists the documents linked to this topic. We also scrap the content of documents for which we think we can do a better job at displaying them than the official document does.

But that's not all: the *“Sem”* in the name is for *“semantic”* (see [“semantic web” on Wikipedia](https://en.m.wikipedia.org/wiki/Semantic_Web)). The database also records the semantic relations between topics. For now the only semantic relation we record is *“A is a subtopic of B”*. For instance, pesticides are a subtopic of agriculture, so if someone searches *“agriculture”* in the search engine and we have document linked to the *“pesticides”* topic, these documents will show up in the results.

This creates what is known as a *knowledge graph* between various topics. Note that this graph is not a *tree* because one node can have several parents: pesticides is both a subtopic of agriculture and of pollution.

For now the task of linking poldocs to topics is done manually, and a good part of the work and source code in this project is dedicated to providing an efficient and comfortable interface for contributing to the database.


## Running Locally

_TODO complete_

```
tools/cmd/up.sh
```

To generate an authentication token:

```
docker-compose -f base.docker-compose.yml -f dev.docker-compose.yml exec web-app node /app/create-token.js "Cédric Van Rompay"
```


## Scrapping

_outdated (TODO update)_

```
tools/scrap.sh --no-older-than 2022-02-10 --legislature 16
```

Then comment out sessions you don't want to scrap.


## Restore from a Backup

```
$ rsync --recursive ~/sempolda/backups/2023-10-29T10-49-20Z root@ovhvps:/root/sempolda/backups/
$ ssh root@ovhvps
root@vps750807:~# cd repos/sempolda/
root@vps750807:~/repos/sempolda#
root@vps750807:~/repos/sempolda# docker compose -p sempolda_staging --file base.docker-compose.yml --file prod.docker-compose.yml run -v /root/sempolda/backups/2023-10-29T10-49-20Z/:/mnt/backup admin reset-from-backup /mnt/backup
root@vps750807:~/repos/sempolda# docker compose -p sempolda_staging --file base.docker-compose.yml --file prod.docker-compose.yml run admin init-system-db
```

Then re-deploy there.


The auto-annotation jobs need an OpenAPI token to do their job.
To update the token, run:

```
rsync --rsync-path="sudo rsync" auto_annotation/.env ubuntu@sempolda.fr:/root/repos/sempolda/auto_annotation/.env
```