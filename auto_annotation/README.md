# Sempolda's Auto Annotation Module

Uses: [NLTK](https://www.nltk.org/), [TF-IDF](https://en.m.wikipedia.org/wiki/Tf%E2%80%93idf) and [OpenAI's GPT](https://openai.com/api/).

Main entry points are:
* the `app` object in `auto_annotation/server.py`, used in the `annotation_suggestions` service
* the script `auto_annotation/__main__.py`, used in the `auto_annotation_job` service
* the script `auto_annotation/recompute_tfidf.py`, used in `tools/recompute-tfidf.py`