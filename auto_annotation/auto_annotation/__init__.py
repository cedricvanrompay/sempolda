import os
from pathlib import Path

_SEMPOLDA_HOME = os.getenv('SEMPOLDA_HOME')

# TODO cleanup directory structure

if _SEMPOLDA_HOME:
    _MAIN_VOLUME = Path(_SEMPOLDA_HOME, "data-volume")
    _TFIDF_VOLUME = Path(_SEMPOLDA_HOME, "local-root/var/lib/sempolda/tfidf")
    _AUTO_ANNOTATION_VOLUME = Path(_SEMPOLDA_HOME, "local-root/var/log/sempolda/auto-annotation-jobs")
else:
    _MAIN_VOLUME = Path("/var/lib/sempolda/main")
    _TFIDF_VOLUME = Path("/var/lib/sempolda/tfidf")
    _AUTO_ANNOTATION_VOLUME = Path("/var/log/sempolda/auto-annotation-jobs")

DB_PATH = _MAIN_VOLUME/"database.db"
SCRAPPED_DATA_DIR_PATH = _MAIN_VOLUME/"scrapped"
TFIDF_JSON_PATH = _TFIDF_VOLUME/"tfidf.json"
AUTO_ANNOTATIONS_LOGS_PATH = _AUTO_ANNOTATION_VOLUME

QUESTION_GOUV_ID_PREFIX = "assemblee-nationale/questions-gouv/"

Topic = str
PoldocID = str

class AutoAnnotationException(Exception):
    """A base class for exceptions from the auto-annotation module."""
