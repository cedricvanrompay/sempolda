import argparse
import binascii
import dataclasses
import datetime
import logging
import os
import json
from pathlib import Path
import sys

from . import AutoAnnotationException
from .scrapped_data import load_interventions, meaningful_text
from .suggestions import CannotPickTopics, get_suggestions, TfidfOnlySuggestions
from .tfidf import TfIdf

from . import (
    QUESTION_GOUV_ID_PREFIX,
    DB_PATH,
    SCRAPPED_DATA_DIR_PATH,
    AUTO_ANNOTATIONS_LOGS_PATH,
    TFIDF_JSON_PATH,
)
from .database import Database, get_connection

MAX_NB_CONSECUTIVE_FAILURE = 5

def main(sessions: list[str]):
    logging.basicConfig(level=logging.INFO)

    try:
        db = Database(get_connection(DB_PATH, read_only=False))
    except Exception as err:
        sys.exit(f'failed to open database at "{DB_PATH}": {err}')

    with open(TFIDF_JSON_PATH) as f:
        tfidf = TfIdf(**json.load(f))
    tfidf.set_stemmer()

    path_to_job_log_file, job_id = _setup_log_file()

    nb_consecutive_failures = 0

    for session in sessions:
        session = Path(
            QUESTION_GOUV_ID_PREFIX,
            session,
        )

        for question_nb in sorted(os.listdir(SCRAPPED_DATA_DIR_PATH/session)):
            poldoc = str(session/question_nb)

            try:
                to_log = _process_one_poldoc(poldoc, db, tfidf, job_id)
                logging.info(f"processed {poldoc}")
                nb_consecutive_failures = 0
            except Exception as err:
                if isinstance(err, CannotPickTopics) and err.decision_data:
                    to_log = {
                        "error": {
                            "msg": str(err),
                            **dataclasses.asdict(err.decision_data),
                        }
                    }
                else:
                    to_log = {
                        "error": str(err)
                    }
                logging.error(f"error processing {poldoc}: {err.__class__.__name__}: {err}")

                nb_consecutive_failures += 1
                if nb_consecutive_failures > MAX_NB_CONSECUTIVE_FAILURE:
                    sys.exit("Too many consecutive failures, stopping early")

            finally:
                with open(path_to_job_log_file, 'a') as f:
                    json.dump({
                        "kind": "poldoc",
                        "poldoc": poldoc,
                        **to_log,
                    }, f, ensure_ascii=False)
                    f.write("\n")


def _setup_log_file():
    start_time = datetime.datetime.now(datetime.timezone.utc)

    random_job_id_suffix = binascii.b2a_hex(os.urandom(8)).decode()

    job_id = "-".join([
        start_time.strftime("%Y-%m-%d-T-%H-%M-%S"),
        random_job_id_suffix,
    ])

    if not AUTO_ANNOTATIONS_LOGS_PATH.is_dir():
        raise RuntimeError(f'missing directory "{AUTO_ANNOTATIONS_LOGS_PATH}"')

    path_to_job_log_file = AUTO_ANNOTATIONS_LOGS_PATH/f"{job_id}.json"

    logging.info(f"log file: {path_to_job_log_file}")

    with open(path_to_job_log_file, 'w') as f:
        json.dump({
            "kind": "start",
            "start_time": start_time.isoformat(timespec="seconds"),
            "random_id_suffix": random_job_id_suffix,
        }, f)
        f.write("\n")

    return path_to_job_log_file, job_id


def _process_one_poldoc(poldoc: str, db: Database, tfidf: TfIdf, job_id: str):
    if db.list_poldoc_direct_topics(poldoc, exclude_auto_annotations=False):
        raise Exception("poldoc is annotated already")

    interventions = load_interventions(SCRAPPED_DATA_DIR_PATH/poldoc)
    text = meaningful_text(interventions)
    title = db.get_poldoc_title(poldoc)

    s = get_suggestions(title, text, tfidf, db)

    # TODO remove when get_suggestions cannot return this type anymore
    if isinstance(s, TfidfOnlySuggestions):
        raise AutoAnnotationException("got a TfidfOnlySuggestions (missing OpenAI key?)")

    db.insert_poldoc_topics(poldoc, s.topics, job_id=job_id)

    to_log = dataclasses.asdict(s)

    return to_log

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("sessions", nargs="+")
    args = parser.parse_args()

    main(sessions=args.sessions)