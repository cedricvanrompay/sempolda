from pathlib import Path
import sqlite3

from . import PoldocID, Topic, AutoAnnotationException

class Database:
    def __init__(self, connection: sqlite3.Connection):
        connection.row_factory = sqlite3.Row
        self.connection = connection

    def list_poldoc_direct_topics(self, poldoc: PoldocID, exclude_auto_annotations: bool):
        return list[str](
            row["label_name"]
            for row in self.connection.execute(f"""
                SELECT label_name FROM docs_labels
                WHERE doc_id=?
                {"AND source_job IS NULL" if exclude_auto_annotations else ""}
            """, (poldoc,)).fetchall()
        )

    def list_all_poldoc_topics(self, poldoc: PoldocID, exclude_auto_annotations: bool):
        all_topics: set[Topic] = set()

        direct_topics = self.list_poldoc_direct_topics(poldoc, exclude_auto_annotations)
        all_topics.update(direct_topics)

        seen = set()
        for topic in direct_topics:
            parents = self.list_topic_parents(topic, seen)
            all_topics.update(parents)

        return all_topics

    def _list_topic_direct_parents(self, topic: Topic):
        rows = self.connection.execute("""
            SELECT parent_label_name
            FROM labels_parent_labels
            WHERE child_label_name = ?
        """, (topic,)).fetchall()

        return [row['parent_label_name'] for row in rows]

    def list_topic_parents(self, topic: Topic, seen=None):
        parents = set()
        seen = seen or set()

        for direct_parent in self._list_topic_direct_parents(topic):
            parents.add(direct_parent)
            if direct_parent not in seen:
                for indirect_parent in self.list_topic_parents(direct_parent, seen):
                    parents.add(indirect_parent)

        return parents

    def list_topic_root_parents(self, topic: Topic):
        return [
            p for p in self.list_topic_parents(topic)
            if not self._list_topic_direct_parents(p)
        ]

    def list_forced_weights(self, token: str):
        rows = self.connection.execute('''
            SELECT label_name, weight
            FROM forced_term_weights
            WHERE term = ?
        ''', (token,)).fetchall()

        return [
            (str(row['label_name']), int(row['weight']))
            for row in rows
        ]

    def get_poldoc_title(self, poldoc: PoldocID):
        row = self.connection.execute(
            '''SELECT title FROM docs WHERE id=?''',
            (poldoc,)
        ).fetchone()

        return row['title']
    
    def _list_topic_direct_children(self, topic: Topic):
        return [
            row["child_label_name"]
            for row in self.connection.execute("""
                SELECT child_label_name FROM labels_parent_labels
                WHERE parent_label_name = ?
            """, (topic,)).fetchall()
        ]

    def list_topic_poldocs(self, topic: Topic, exclude_auto_annotations: bool,  seen_topics: set | None=None):
        poldocs = {
            row["doc_id"]
            for row in self.connection.execute(f"""
                SELECT doc_id FROM docs_labels
                WHERE label_name = ?
                {"AND source_job IS NULL" if exclude_auto_annotations else ""}
            """, (topic,)).fetchall()
        }

        seen_topics = seen_topics or set()

        for t in self._list_topic_direct_children(topic):
            if t in seen_topics:
                continue

            poldocs.update(self.list_topic_poldocs(t, exclude_auto_annotations, seen_topics))

            seen_topics.add(t)

        return poldocs

    def insert_poldoc_topics(self, poldoc: PoldocID, topics: list[Topic], *, job_id: str):
        """insert using a transaction (all or none inserted)
        
        will fail if the DB connection is read-only
        """

        if job_id == "":
            raise ValueError(
                "job_id cannot be the empty string."
                " Inserting without a job ID is not implemented yet."
            )

        # will commit the changes at the end of the "with" block
        # and roll them back in case of failure
        with self.connection:
            for topic in topics:
                self.connection.execute("""
                    INSERT INTO docs_labels (doc_id, label_name, source_job)
                    VALUES (?, ?, ?)
                """, (poldoc, topic, job_id))

    def close(self):
        self.connection.close()

def get_connection(path: Path | str, read_only: bool=True):
    path = Path(path)

    if not path.exists():
        raise AutoAnnotationException("path does not exist")

    connection_string = f"file:{path}"

    if read_only:
        connection_string += "?mode=ro"

    return sqlite3.connect(
        connection_string,
        uri=True,
        # see https://docs.python.org/3/library/sqlite3.html#sqlite3-transaction-control-autocommit
        autocommit=False,
    )