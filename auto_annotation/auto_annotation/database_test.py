import sqlite3

from .database import Database

def get_test_db():
    # the path must be relative to the CWD where pytest will be executed
    with open("../databases/main/schema.sql") as f:
        schema = f.read()

    test_db = sqlite3.connect(":memory:")
    test_db.executescript(schema)

    return test_db

def test_list_topic_parents():
    test_db = get_test_db()

    for parent, child in [
        ('agriculture', 'pesticides'),
        ('pollution', 'pesticides'),
        ('environnement', 'pollution'),
        ('pollution', 'dioxide de carbone'),
        ('agriculture', 'PAC (Politique Agricole Commune)')
    ]:
        test_db.execute("""
            INSERT INTO labels_parent_labels
                (parent_label_name, child_label_name)
            VALUES (?, ?)
        """, (parent, child))

    d = Database(test_db)

    parents = d.list_topic_parents('pesticides')

    # TODO test that no label was visited more than once

    assert parents == {'agriculture', 'pollution', 'environnement'}

def test_list_all_poldoc_topics():
    test_db = get_test_db()

    test_db.execute("INSERT INTO docs (id, title) VALUES ('abc', 'test doc')")
    test_db.execute('INSERT INTO labels (name) VALUES ("foo"), ("bar"), ("baz")')
    test_db.execute('INSERT INTO docs_labels (doc_id, label_name) VALUES ("abc", "foo")')
    test_db.execute("""
        INSERT INTO docs_labels (doc_id, label_name, source_job)
        VALUES ("abc", "bar", "fake-job"), ("abc", "baz", "fake-job")
    """)

    assert (
        set(Database(test_db).list_all_poldoc_topics("abc", exclude_auto_annotations=False))
        == {"foo", "bar", "baz"}
    )

    assert (
        set(Database(test_db).list_all_poldoc_topics("abc", exclude_auto_annotations=True))
        == {"foo"}
    )