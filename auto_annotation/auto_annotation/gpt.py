import logging
import os

import openai

from . import AutoAnnotationException

logger = logging.getLogger(__name__)

openai.organization = "org-qIAKbgyWCNjqzpmJamAO1FpH"
openai.api_key = os.getenv("OPENAI_API_KEY")
if not openai.api_key:
    logging.warning('no OpenAI API key; will not be able to call GPT')

INSTRUCTIONS = '''\
Tu aides à détecter des erreurs de classification de documents politiques.

Tes réponses sont toujours soit "aucuns", soit une liste de sujet au format suivant:
- sujet 1
- sujet 2
- etc...
'''

class NoToken(AutoAnnotationException):
  def __init__(self) -> None:
    super().__init__("Cannot use the OpenAI GPT API because API token is missing")

def refine_topics(text: str, candidate_topics: list[str], instructions:str=INSTRUCTIONS):
    messages = [
        {"role": "system", "content": instructions},
        {"role": "user", "content": "Voici un texte qu'on cherche à classifier :\n"+text},
    ]

    messages.append({
       "role": "user",
       "content": "\n".join([
            "Parmis les sujets suivants, lesquels ne s'apppliquent *pas* au texte ?",
            *["- "+topic for topic in candidate_topics],
        ])
    })

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages,
    )
    response_message = response.choices[0].message #pyright: ignore
    excluded_topics = _parse_gpt_output(response_message.content)

    messages.append(response_message)

    messages.append({
       "role": "user",
       "content": (
          "et est-ce qu'il y en a un petit nombre (maximum trois) "
          "qui au contraire correspondent beaucoup plus au texte que les autres ?"
       ),
    })

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages,
    )
    response_message = response.choices[0].message #pyright: ignore
    relevant_topics = _parse_gpt_output(response_message.content)

    messages.append(response_message)

    return excluded_topics, relevant_topics

def _parse_gpt_output(raw_output: str):
  if raw_output.lower() == "aucuns":
    return []

  if raw_output.startswith('- '):
    keywords = raw_output.lstrip('- ').split('\n- ')
    return keywords

  # even though we ask GPT a particular format,
  # it regularly uses a different format,
  # but it can still be parsed most of the time
  
  lines = raw_output.split('\n')
  if len(lines) > 2:
    if all(l.startswith("-") for l in lines[1:]):
      # typically, one line of explanation and then the list
      return [l.lstrip('- ') for l in lines[:1]]
    
    if not any(l.startswith('-') for l in lines):
      # just one topic per line maybe?
      return lines
    
    raise AutoAnnotationException(f'malformed output from GPT: "{raw_output}"')

  keywords = raw_output.split(", ")
  if len(keywords) > 2:
    return keywords
  
  raise AutoAnnotationException(f'malformed output from GPT: "{raw_output}"')
  