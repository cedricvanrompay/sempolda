import re
import nltk

_TO_REMOVE = re.compile("…")

def get_tokens(text: str):
    for t in nltk.tokenize.word_tokenize(text, language="french"):
        t = re.sub(_TO_REMOVE, "", t)

        if _ignore_token(t):
            continue

        yield t

def _ignore_token(t):
    if (
        t in nltk.corpus.stopwords.words('french')
        or len(t) <= 1
        or t in {"M."}
    ):
        return True
    else:
        return False
