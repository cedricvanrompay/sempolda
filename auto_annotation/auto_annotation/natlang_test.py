import json
from .scrapped_data import _load_interventions
from .natlang import get_tokens

def test_get_tokens():
    with open("../scrapper/testdata/2022-07-12/01.json") as f:
        interventions = _load_interventions(json.load(f))

    with open("test-data/tokens.txt") as f:
        expected = [line.strip() for line in f.readlines()]

    text = '\n\n'.join(
        i.texte
        for i in interventions
    )

    assert list(get_tokens(text)) == expected
