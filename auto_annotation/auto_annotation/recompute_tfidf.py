import dataclasses
import json
import logging

from . import (
    SCRAPPED_DATA_DIR_PATH,
    DB_PATH,
    TFIDF_JSON_PATH,
    PoldocID,
)
from .tfidf import TfIdf
from .natlang import get_tokens
from .database import Database, get_connection
from .scrapped_data import load_interventions, meaningful_text

# calling basicConfig is required
# if you create a new logger instead of the default logger
# as we do here
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

questions_gouv_path = SCRAPPED_DATA_DIR_PATH/'assemblee-nationale/questions-gouv/'

tfidf = TfIdf()
tfidf.set_stemmer()

db = Database(get_connection(DB_PATH))

for date_path in questions_gouv_path.iterdir():
    logger.info(f"processing {date_path.name}")

    for poldoc_path in date_path.iterdir():
        poldoc_id = PoldocID(poldoc_path.relative_to(SCRAPPED_DATA_DIR_PATH))

        try:
            interventions = load_interventions(SCRAPPED_DATA_DIR_PATH/poldoc_id)
            title = db.get_poldoc_title(poldoc_id)
        except Exception as error:
            logger.warning(f"processing {poldoc_id}: {repr(error)}")
            continue

        topics = db.list_all_poldoc_topics(poldoc_id, exclude_auto_annotations=True)

        # TODO factorize this out with the same code from server.py
        text = (
            meaningful_text(interventions)
            # to give the words in the title more weight,
            # we simply repeat the title 5 times
            + " ".join([title]*5)
        )

        tfidf.add_document(topics, get_tokens(text))

with open(TFIDF_JSON_PATH, 'w') as f:
    json.dump(dataclasses.asdict(tfidf), f, ensure_ascii=False)
