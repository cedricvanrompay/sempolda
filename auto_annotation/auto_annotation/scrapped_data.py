# TODO use this file in the scrapper too;
# to make sure the same file is used
# by both the scrapper and the auto annotation service,
# we can simply use a test for now comparing the two files,
# and we will use something more sophisticated later
# like a separate docker image

from dataclasses import dataclass
import json
from pathlib import Path
from typing import Optional

import bs4

from . import AutoAnnotationException

@dataclass
class Intervention:
    texte: str
    orateur: str
    paragraphs: Optional[list[str]] = None
    type: Optional[str] = None

def load_interventions(poldoc_dir: Path | str):
    interventions: list[Intervention] = list()

    poldoc_dir = Path(poldoc_dir)

    if (poldoc_dir/'content.json').exists():
        with open(poldoc_dir/'content.json') as f:
            # old format with text being only one paragraph
            for intervention in json.load(f):
                interventions.append(Intervention(**intervention))
    elif (poldoc_dir/'interventions.json').exists():
        with open(poldoc_dir/'interventions.json') as f:
            # new format with paragraphs
            return _load_interventions(json.load(f))
    else:
        raise InterventionsFileNotFound(poldoc_dir)

    return interventions


def _load_interventions(json_data):
    """must only be used with recent format with a "paragraphs" section"""
    interventions = list[Intervention]()

    for i, x in enumerate(json_data):
        if not x["paragraphs"]:
            raise ValueError(f'no "paragraph" section in element at index {i}: {str(x)[:20]}')
        
        # recent versions of the scrapper put crowd reactions in <i></i>;
        # note that sometimes these tags end up being used for other things than crowd reactions,
        # for instance for "Journal officiel" in assemblee-nationale/questions-gouv/2022-11-22/20
        x['texte'] = remove_italic(
            "\n\n".join(x['paragraphs'])
        )

        interventions.append(Intervention(**x))

    return interventions


def remove_italic(text: str):
    soup = bs4.BeautifulSoup(text, 'lxml')

    for x in soup.find_all('i'):
        # "extract" removes the element from the soup
        x.extract()

    # removing tags can cause double-spaces to appear
    return soup.text.replace('  ', ' ')

def meaningful_text(interventions: list[Intervention]):
    return "\n\n".join([
        intervention.texte
        for intervention in interventions
        if intervention.type in [
            "question_or_answer",
            # interventions parsed some time ago
            # don't have a "type"
            None,
        ]
    ])

class InterventionsFileNotFound(AutoAnnotationException):
    def __init__(self, poldoc_dir: Path):
        super().__init__(' '.join([
            f'interventions file not found in {poldoc_dir}',
            f'(directory content: {list(poldoc_dir.iterdir())})',
        ]))