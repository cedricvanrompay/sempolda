import http
import json
from flask import Flask, g, jsonify, request
import logging

import werkzeug.exceptions

from . import (
    DB_PATH,
    TFIDF_JSON_PATH,
    SCRAPPED_DATA_DIR_PATH,
    QUESTION_GOUV_ID_PREFIX,
    PoldocID,
)
from .tfidf import TfIdf
from .database import Database, get_connection
from .scrapped_data import load_interventions, meaningful_text
from . import suggestions

# note: stdout does not seem to be captured in the container logs for some reason,
# so print ,ust be used with "file=sys.stderr"
logging.basicConfig(level=logging.INFO)

with open(TFIDF_JSON_PATH) as f:
    tfidf = TfIdf(**json.load(f))

tfidf.set_stemmer()

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = Database(get_connection(DB_PATH))
        g._database = db
    return db

app = Flask(__name__)

def make_json_response(*args, **kwargs):
    if kwargs:
        raise Exception("unexpected keyword arguments")
    if len(args) != 1:
        raise Exception(f"expected 1 argument, got {len(args)}")
    
    x = args[0]

    if isinstance(x, werkzeug.exceptions.HTTPException):
        result = jsonify({
            "error": {
                "type": str(type(x)),
                "message": str(x),
            }
        })
        result.status_code = x.code or 500
    elif isinstance(x, Exception):
        result = jsonify({
            "error": {
                "type": str(type(x)),
                "message": str(x),
            }
        })
        result.status_code = 500
    else:
        result = jsonify(x)

    if request.path != "/health":
        app.logger.info(f" {result.status_code} {request.path}")

    return result

# always return a JSON object
app.make_response = make_json_response

@app.route("/")
def hello_world():
    return "Hi from auto annotation service"

@app.route("/health")
def get_health():
    return {}

# type checker is confused because of jsonify being called in make_json_response instead
@app.route("/suggestions/<path:id>")  # type: ignore
def get_suggestions(id: PoldocID):
    db = get_db()

    if id.startswith(QUESTION_GOUV_ID_PREFIX):
        interventions = load_interventions(SCRAPPED_DATA_DIR_PATH/id)
        text = meaningful_text(interventions)
    else:
        return "poldoc ID does not start with expected prefix", http.HTTPStatus.BAD_REQUEST

    title = db.get_poldoc_title(id)

    try:
        if "tfidf-only" in request.args:
            return suggestions.get_tfidf_only_suggestions(title, text, tfidf, db)
        else:
            return suggestions.get_suggestions(title, text, tfidf, db)
    except suggestions.CannotPickTopics as error:
        if not error.decision_data:
            # XXX should not happen in practice, this is only for the type checker
            # TODO make it clear for the type checker that it cannot happen
            raise Exception('"CannotPickTopics" error without decision data')
            
        return suggestions.Suggestions(
            topics=[],
            reason=error.reason,
            decision_data=error.decision_data,
        )

@app.route("/tfidf/topic-terms/<string:topic>")
def get_topic_info(topic):
    return tfidf.get_topic_terms(topic)

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
