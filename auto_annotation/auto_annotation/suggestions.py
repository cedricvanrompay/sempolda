from dataclasses import dataclass
import typing
from typing import Optional


from . import (
    Topic,
    AutoAnnotationException,
)
from . import tfidf
from . import gpt
from .scrapped_data import Intervention, meaningful_text
from .database import Database

# score below which all topics are rejected
MIN_SCORE = 20
# score above which all topics are accepted
MIN_SCORE_2 = 32
MAX_TOPICS = 5
GPT_BOOST_PERCENT = 40


@dataclass
# XXX unused?
class Candidate:
    tfidf: tfidf.TopicScore
    gpt_decision: typing.Literal["excluded", "relevant"]
    final_score: float

def get_tfidf_only_suggestions(title: str, text: str, tfidf: tfidf.TfIdf, db: Database):
    return TfidfOnlySuggestions(tfidf.get_top_score_topics(text, title, db))

def get_suggestions(title: str, text: str, tfidf: tfidf.TfIdf, db: Database):
    tfidf_suggestions = tfidf.get_top_score_topics(text, title, db)

    excluded_topics, relevant_topics = gpt.refine_topics(
        title+"\n"+text,
        [topic for topic in tfidf_suggestions],
    )

    final_scores = list[typing.Tuple[float, Topic]](
        (
            (
                tfidf_score.score * (100 + GPT_BOOST_PERCENT)/100 if topic in relevant_topics
                else 0 if topic in excluded_topics
                else tfidf_score.score
            ),
            topic,
        ) for topic, tfidf_score in tfidf_suggestions.items()
    )

    decision_data = SuggestionDecisionData(tfidf_suggestions, excluded_topics, relevant_topics, final_scores)

    try:
        picked_topics, reason = _pick_top_topics(final_scores, db)
    except CannotPickTopics as err:
        err.decision_data = decision_data
        err.add_note(f"{final_scores = }")

        raise err from None

    return Suggestions(picked_topics, reason, decision_data)

@dataclass
class TfidfOnlySuggestions:
    tfidf_suggestions: dict[Topic, tfidf.TopicScore]

@dataclass
class SuggestionDecisionData:
    tfidf_suggestions: dict[Topic, tfidf.TopicScore]
    excluded_topics: list[str]
    relevant_topics: list[str]
    final_scores: list[typing.Tuple[float, Topic]]

@dataclass
class Suggestions:
    topics: list[str]
    reason: str
    decision_data: SuggestionDecisionData

def _pick_top_topics(final_scores: list[typing.Tuple[float, Topic]], db: Database):
    # sorted from highest to lowest score
    final_scores.sort(reverse=True)

    top_score, single_top_topic = final_scores[0]

    if top_score < MIN_SCORE:
        raise CannotPickTopics(
            f"highest score ({top_score:.2f} for \"{single_top_topic}\") "
            f"is below the minimum ({MIN_SCORE})"
        )

    min_score_for_top_topics = max(
        min(top_score * 0.8, MIN_SCORE_2),
        MIN_SCORE,
    )

    top_topics = {
        t for (score, t) in final_scores
        if score > min_score_for_top_topics
    }

    if len(top_topics) == 1:
        reason = (
            f"only topic above the mininum ({MIN_SCORE})" if min_score_for_top_topics == MIN_SCORE
            else f"only top topic (no other topic above {min_score_for_top_topics})"
        )
        return [single_top_topic], reason

    removed_single_poldoc_topics = set()
    for t in top_topics:
        try:
            # topics that have only one (human) annotation
            # are not very reliable when it comes to TF-IDF
            poldocs = db.list_topic_poldocs(t, exclude_auto_annotations=True)
            if len(poldocs) <= 1:
                removed_single_poldoc_topics.add(t)
                # note that we must not alter top_topics while we are iterating on it
        except Exception as err:
            err.add_note(f"{t = }")
            raise err from None
    
    picked = top_topics - removed_single_poldoc_topics

    if len(picked) < 1:
        raise CannotPickTopics(f"all top topics where removed: {removed_single_poldoc_topics = }")
    
    if len(top_topics) > MAX_TOPICS:
        raise CannotPickTopics(f"too many top topics ({top_topics})")

    if removed_single_poldoc_topics:
        reason = (
            "the following top topics got removed"
            f" because they only had one associated poldoc: {removed_single_poldoc_topics}"
        )
    else:
        reason = "top topics (none removed)"

    return list(picked), reason


class CannotPickTopics(AutoAnnotationException):
    reason: str

    decision_data: Optional[SuggestionDecisionData]

    def __init__(self, reason: str) -> None:
        super().__init__("Cannot find topics suitable for annotation: "+reason)
        self.reason = reason

