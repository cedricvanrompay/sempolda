import json

from .natlang import get_tokens
from .tfidf import TermContribution, TfIdf, TopicScore


def test_poldoc_suggestions():
    with open("test-data/tfidf.json") as f:
        tfidf = TfIdf(**json.load(f))

    tfidf.set_stemmer()
    s = tfidf.get_top_score_topics(
        text="et si on parlait de la réforme des retraites ?",
        title="",
    )

    assert s == {
        'pénibilité du travail': TopicScore(
            score=0.12386994568299246,
            tokens_contributions=[
                TermContribution(
                    term='réform',
                    weigth=0.12386994568299246,
                    occurences=1,
                    contribution=0.12386994568299246,
                ),
            ],
        ),
        'réforme 2023 des retraites': TopicScore(
            score=0.22814886770125875,
            tokens_contributions=[
                TermContribution(
                    term='réform',
                    weigth=0.22814886770125875,
                    occurences=1,
                    contribution=0.22814886770125875,
                ),
            ],
        ),
        'test': TopicScore(
            score=0.16515992757732328,
            tokens_contributions=[
                TermContribution(
                    term='réform',
                    weigth=0.16515992757732328,
                    occurences=1,
                    contribution=0.16515992757732328,
                ),
            ],
        ),
    }
    

def test_add_document():
    with open('test-data/documents.json') as f:
        documents = json.load(f)

    tfidf = TfIdf()
    tfidf.set_stemmer()
    for d in documents:
        tfidf.add_document(d['topics'], get_tokens(' '.join(d['text'])))

    with open('test-data/tfidf.json') as f:
        expected = TfIdf(**json.load(f))

    assert [
        (term, count)
        for (term, count) in tfidf.document_per_term.items()
        if (count > len(documents))
    ] == []

    assert tfidf == expected