from .scrapped_data import remove_italic

def test_remove_italic():
    assert remove_italic('lorem ipsum <i>dolor sit</i> amet') == "lorem ipsum amet"