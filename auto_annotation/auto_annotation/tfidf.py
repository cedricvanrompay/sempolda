from collections import Counter
from dataclasses import dataclass, field
import math
from typing import Iterable, Optional, Tuple

from nltk.stem.snowball import FrenchStemmer


from . import Topic
from . import natlang
from .database import Database

DEFAULT_STEMMER = FrenchStemmer()

# these are not really "terms"
# (more like token stems)
# but "term" is the word used to make the "TF-IDF" acronym
Term = str

@dataclass
class TermContribution:
    term: str
    weigth: float
    occurences: int
    contribution: float

@dataclass
class TopicScore:
    score: float
    tokens_contributions: list[TermContribution]

@dataclass
class TfIdf:
    # XXX there is some redundancy between some of these fields
    total_nb_documents: int = 0
    term_occurrences: dict[Term, dict[Topic, int]] = field(default_factory=dict)
    document_per_term: dict[Term, int] = field(default_factory=dict)
    documents_per_topic: dict[Topic, int] = field(default_factory=dict)
    document_per_term_per_topic: dict[Term, dict[Topic, int]] = field(default_factory=dict)
    terms_per_topic: dict[Topic, int] = field(default_factory=dict)

    # we are not using the "__init__" method
    # because this is a dataclass
    def set_database(self, db: Database):
        """optional (will enable forced weights)"""
        self.database = db

    # TODO add type for stemmer
    def set_stemmer(self, stemmer=DEFAULT_STEMMER):
        self.stemmer = stemmer

    # TODO take a Iterable[Term] as input instead?
    def add_document(self, topics: Iterable[Topic], tokens: Iterable[str]):
        if type(tokens) == str:
            raise TypeError('tokens must be an iterator of strings, but not a string')

        for topic in topics:
            self.documents_per_topic[topic] = (
                self.documents_per_topic.get(topic, 0)
                + 1
            )

        seen_terms = set()

        for token in tokens:
            term = self.stemmer.stem(token)

            # TODO try using default dicts

            if term not in seen_terms:
                if term not in self.document_per_term:
                    self.document_per_term[term] = 0
                self.document_per_term[term] += 1

            if term not in self.term_occurrences:
                self.term_occurrences[term] = dict()

            for topic in topics:
                self.term_occurrences[term][topic] = (
                    self.term_occurrences[term].get(topic, 0)
                    + 1
                )

                self.terms_per_topic[topic] = (
                    self.terms_per_topic.get(topic, 0)
                    + 1
                )

                if term not in seen_terms:
                    if term not in self.document_per_term_per_topic:
                        self.document_per_term_per_topic[term] = dict()

                    self.document_per_term_per_topic[term][topic] = (
                        self.document_per_term_per_topic[term].get(topic, 0)
                        + 1
                    )
            
            seen_terms.add(term)

        self.total_nb_documents += 1

    def get_top_score_topics(self, text: str, title: str, database: Optional[Database]=None):
        """returns the top 10 topics by TF-IDF score"""
        tokens = natlang.get_tokens(
            text
            # to give the words in the title more weight,
            # we simply repeat the title 5 times.
            # Note that we don't use "join"
            # because we need a space at the beginning
            + 5*(" "+title)
        )

        contributions = self._get_contributions(tokens, database)

        topics_scores = [
            (topic, score_from_contributions(contributions[topic]))
            for topic in contributions
        ]

        return {
            topic: score
            # it seems that the ordering is not preserved
            # through encoding-decoding,
            # so maybe we should only do it in the browser
            for (topic, score) in sorted(
                topics_scores,
                key=(lambda x: x[1].score),
                reverse=True,
            )[:10]
        }

    def get_tfidf_details_for_topic(self, topic: Topic, tokens: Iterable[Term], database: Optional[Database]=None):
        contributions = self._get_contributions(tokens, database)

        return score_from_contributions(contributions[topic])

    def get_topic_terms(self, topic: Topic):
        return sorted(
            [
                (self._compute_one_tf_idf_weight(token, topic), token)
                for token in self.term_occurrences
                if topic in self.term_occurrences[token]
            ],
            reverse=True,
        )

    def _compute_one_tf_idf_weight(self, token: str, topic: str):
        # frequency of this term accross all documents with this topic
        tf = self.term_occurrences[token][topic] / self.terms_per_topic[topic]

        # ratio of documents containing this term
        # among documents NOT related to this topic;
        # this represents whether this term is typical of this topic
        # or is rather generic

        documents_without_this_topic = (
            self.total_nb_documents
            - self.documents_per_topic[topic]
        )
        documents_with_term_without_topic = (
            self.document_per_term[token]
            - self.document_per_term_per_topic[token][topic]
        )
        # the +1 is to avoid divisions by zero and domain errors
        idf = (documents_without_this_topic + 1) / (documents_with_term_without_topic + 1)

        # we use the logarithm of the IDF
        # as advised in https://en.wikipedia.org/wiki/Tf%E2%80%93idf#Inverse_document_frequency;
        # we also throw in the number of documents for this topic
        # (with a bit of hand-wavy heuristics)
        # to avoid weird results from topics that only match one document
        return tf*math.log(idf * math.log(math.e + self.documents_per_topic[topic]))

    def _compute_tf_idf_weights(self, token: str):
        if token not in self.term_occurrences:
            return dict[Topic, float]()
        
        return {
            topic: self._compute_one_tf_idf_weight(token, topic)
            for topic in self.term_occurrences[token]
        }

    # database is not an instance attribute
    # because we will need a different database connection for each worker thread
    # in the Web service
    def _get_contributions(self, tokens: Iterable[Term], database: Optional[Database]):
        # caching TF-IDF weights
        # only for this query
        # XXX is this the best strategy?
        # XXX key seems like it's a "stem" not a term/token
        tfidf_weights = dict[Term, dict[Topic, float]]()

        contributions = dict[Topic, Counter[Tuple[Term, float]]]()

        def add_one_contribution(topic: str, token: str, weight: float):
            if topic not in contributions:
                contributions[topic] = Counter[Tuple[Term, float]]()

            # multiplying by 10 to make values more readable
            contributions[topic].update([(token, weight*10)])

        for token in tokens:
            if database:
                for topic, weight in database.list_forced_weights(token):
                    add_one_contribution(topic, token, weight)
                    continue

            stem = self.stemmer.stem(token)

            if stem not in tfidf_weights:
                tfidf_weights[stem] = self._compute_tf_idf_weights(stem)

            for topic, weight in tfidf_weights[stem].items():
                add_one_contribution(topic, stem, weight)

        return contributions

def score_from_contributions(contribs: Counter[Tuple[Term, float]], contrib_score_cutoff_ratio=0.8):
    """compute score by summing all contributions
    
    token contribution details will only be included for the highest contributions
    summing to contrib_score_cutoff_ratio times the total score.
    """
    score = 0
    contributions = list[TermContribution]()

    for (token, weight), count in contribs.items():
        c = weight * count
        contributions.append(TermContribution(token, weight, count, c))
        score += c

    biggest_contribs = list[TermContribution]()
    s = 0 # cumulative score
    cutoff = score * contrib_score_cutoff_ratio
    for c in sorted(
            contributions,
            # sort by biggest contribution first
            key=lambda x: x.contribution,
            reverse=True,
        ):
        if s > cutoff:
            break
        biggest_contribs.append(c)
        s += c.contribution

    return TopicScore(
        score,
        biggest_contribs,
    )
