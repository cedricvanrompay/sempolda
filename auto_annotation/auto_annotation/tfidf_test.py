import dataclasses
import json

from .tfidf import TfIdf

def test_encoding_decoding():
    with open('test-data/tfidf.json') as f:
        # stripping the new line
        # that VS code tends to add automatically at the end
        encoded = f.read().strip()

    decoded = TfIdf(**json.loads(encoded))

    re_encoded = json.dumps(dataclasses.asdict(decoded), indent=4, ensure_ascii=False)

    assert re_encoded == encoded

# TODO test forced weights
