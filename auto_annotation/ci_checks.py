import os
import tomllib


with open("Pipfile", "rb") as f:
    pipfile = tomllib.load(f)

    python_version = pipfile["requires"]["python_version"]

real_path = os.path.realpath("Dockerfile")

with open('Dockerfile') as f:
    for line_nb, line in enumerate(f, start=1):
        if not line.startswith("FROM"):
            continue

        if not line.startswith(f"FROM python:{python_version}"):
            exit(" ".join([
                "ERROR:",
                f"in {real_path}, line {line_nb}:",
                f"expected Python version {python_version},",
                f"found {line}"
            ]))