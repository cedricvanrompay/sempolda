# TODO delete?

# to use with:
#
# $ git checkout 32dc07b
# $ docker build -t sempolda_annotation_suggestions:32dc07b9 annotation_suggestions/
# $ docker run --volumes-from sempolda_annotation_suggestions_1 --detach --rm --name sempolda_annotation_suggestions_old --publish 127.0.0.1:8080:80
# 27 --network sempolda_default sempolda_annotation_suggestions:32dc07b9
# $ docker run --volumes-from sempolda_annotation_suggestions_1 --detach --rm --name sempolda_annotation_suggestions --publish 127.0.0.1:8081:8027 -
# -network sempolda_default sempolda_annotation_suggestions:latest

import requests

def compare(doc_id: str):
    suggestions = dict()

    for port, key in [[8081, 'latest'], [8080, 'old']]:
        url = f'http://localhost:{port}/suggestions/{doc_id}'
        data = requests.get(url).json()

        for each in data['suggestions']:
            topic_name = each['topic_name']
            if topic_name not in suggestions:
                suggestions[topic_name] = dict()
            suggestions[topic_name][key] = each['score']

    return suggestions
