import shutil
import logging
import sqlite3

logging.basicConfig(level=logging.INFO)

def progress(status, remaining, total):
    if (total-remaining) % 10 == 0:
        logging.info(f'Copied {total-remaining} of {total} pages...')

db = sqlite3.connect('file:/var/lib/sempolda/database.db?mode=ro', uri=True)
backup = sqlite3.connect('/mnt/backup/database.db')

with backup:
    db.backup(backup, pages=1, progress=progress)

db.close()
backup.close()

logging.info("copying scrapped data...")
shutil.copytree('/var/lib/sempolda/scrapped', '/mnt/backup/scrapped')

logging.info("copying auto-annotation jobs logs...")
shutil.copytree("/var/log/sempolda/auto-annotation-jobs", "/mnt/backup/auto-annotation-jobs")