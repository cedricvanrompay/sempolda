import argparse
import os
import sqlite3
import tempfile

def create_new_db(path_to_source_db: str, path_to_dest_db: str, path_to_schema):
    source_db = sqlite3.connect(f'file:{path_to_source_db}?mode=ro', uri=True)
    source_db.row_factory = sqlite3.Row

    destination_db = sqlite3.connect(path_to_dest_db)

    with open(path_to_schema) as f:
        schema = f.read()
    destination_db.executescript(schema)

    rows = (source_db
        .execute("SELECT name, description FROM topics")
        .fetchall()
    )
    for row in rows:
        destination_db.execute(
            "INSERT INTO labels (name, description) VALUES (?, ?)",
            (row['name'], row['description']),
        )

    rows = (source_db
        .execute("SELECT id, slug, title, official_source_url FROM poldocs")
        .fetchall()
    )
    for row in rows:
        doc_id = 'assemblee-nationale/questions-gouv/'+row['slug']

        destination_db.execute(
            "INSERT INTO docs (id, title) VALUES (?, ?)",
            (doc_id, row['title'])
        )

        destination_db.execute(
            "INSERT INTO poldocs (doc_id, official_source_url) VALUES (?, ?)",
            (doc_id, row['official_source_url']),
        )

        matched_topics_rows = (source_db
            .execute("""
                SELECT topics.name
                    FROM poldocs_matched_topics
                    JOIN topics
                    WHERE poldocs_matched_topics.topic_id = topics.id
                    AND poldocs_matched_topics.poldoc_id = ?
            """, (row['id'],))
            .fetchall()
        )
        for row in matched_topics_rows:
            destination_db.execute(
                "INSERT INTO docs_labels (doc_id, label_name) VALUES (?, ?)",
                (doc_id, row['name']),
            )

    for row in (source_db
        .execute("""
            SELECT parent.name, child.name FROM topics_matched_topics
                JOIN topics AS parent JOIN topics AS child
                WHERE topics_matched_topics.subtopic_id = child.id
                AND topics_matched_topics.parent_topic_id = parent.id
        """)
        .fetchall()
    ):
        destination_db.execute(
            "INSERT INTO labels_parent_labels (child_label_name, parent_label_name) VALUES (?, ?)",
            # using indexes instead of string keys
            # because both are under key "name"
            (row[1], row[0])
        )


    destination_db.commit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--source')
    parser.add_argument('--destination', required=False)
    parser.add_argument('--schema')

    args = parser.parse_args()

    if not args.destination:
        tmpdir = tempfile.mkdtemp()
        destination = os.path.join(tmpdir, "database.db")
        print("destination DB:", destination)
    else:
        destination = args.destination

    create_new_db(args.source, destination, args.schema)