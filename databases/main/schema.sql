CREATE TABLE labels (
    name TEXT PRIMARY KEY,
    description TEXT
);

CREATE TABLE synonyms (
    name TEXT PRIMARY KEY,
    label_name NOT NULL REFERENCES labels(name) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE docs (
    id TEXT PRIMARY KEY,
    title TEXT NOT NULL,
    official_source_url TEXT,
    -- TODO make it NOT NULL
    -- but this requires a trick,
    -- see https://gitlab.com/cedricvanrompay/sempolda/-/merge_requests/218#note_2259520579
    date TEXT
);

CREATE TABLE docs_labels (
    doc_id NOT NULL REFERENCES docs(id),
    label_name NOT NULL REFERENCES labels(name) ON UPDATE CASCADE ON DELETE CASCADE,
    source_job TEXT,
    PRIMARY KEY (doc_id, label_name)
);

CREATE TABLE labels_parent_labels (
    child_label_name NOT NULL REFERENCES labels(name) ON UPDATE CASCADE ON DELETE CASCADE,
    parent_label_name NOT NULL REFERENCES labels(name) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY (child_label_name, parent_label_name)
);

CREATE TABLE forced_term_weights (
    term TEXT NOT NULL,
    label_name NOT NULL REFERENCES labels(name) ON UPDATE CASCADE ON DELETE CASCADE,
    weight INTEGER NOT NULL,
    UNIQUE(term, label_name)
);

CREATE TABLE persons (
    id TEXT PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE assemble_nationale_acteurs_persons (
    id_acteur TEXT NOT NULL,
    person_id NOT NULL REFERENCES persons(id),
    UNIQUE (id_acteur),
    UNIQUE (person_id)
);

CREATE TABLE docs_persons (
    doc_id NOT NULL REFERENCES docs(id) ON DELETE CASCADE,
    person_id NOT NULL REFERENCES persons(id) ON UPDATE CASCADE ON DELETE CASCADE
);