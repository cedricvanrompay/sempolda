```
$ docker run --rm -i grafana/k6 run --vus 30 --duration 30s - < load-testing.js

[...]

     ✓ status was 200

     checks.........................: 100.00% ✓ 4311       ✗ 0
     data_received..................: 97 MB   3.2 MB/s
     data_sent......................: 477 kB  16 kB/s
     http_req_blocked...............: avg=753.86µs min=420ns    med=1.5µs    max=124.46ms p(90)=2.15µs   p(95)=2.42µs
     http_req_connecting............: avg=83.07µs  min=0s       med=0s       max=15.25ms  p(90)=0s       p(95)=0s
     http_req_duration..............: avg=209.21ms min=21.48ms  med=186.19ms max=2.91s    p(90)=296.94ms p(95)=364.47ms
       { expected_response:true }...: avg=209.21ms min=21.48ms  med=186.19ms max=2.91s    p(90)=296.94ms p(95)=364.47ms
     http_req_failed................: 0.00%   ✓ 0          ✗ 4311
     http_req_receiving.............: avg=2.12ms   min=36.42µs  med=1.01ms   max=23.91ms  p(90)=8.2ms    p(95)=9.18ms
     http_req_sending...............: avg=187.6µs  min=48.82µs  med=180.72µs max=1.13ms   p(90)=235.21µs p(95)=256.53µs
     http_req_tls_handshaking.......: avg=467.15µs min=0s       med=0s       max=80.61ms  p(90)=0s       p(95)=0s
     http_req_waiting...............: avg=206.89ms min=19.8ms   med=184.31ms max=2.9s     p(90)=292.19ms p(95)=360.22ms
     http_reqs......................: 4311    141.935895/s
     iteration_duration.............: avg=631.66ms min=176.38ms med=575.2ms  max=3.56s    p(90)=864.06ms p(95)=999.56ms
     iterations.....................: 1437    47.311965/s
     vus............................: 30      min=30       max=30
     vus_max........................: 30      min=30       max=30


running (0m30.4s), 00/30 VUs, 1437 complete and 0 interrupted iterations
default ✓ [ 100% ] 30 VUs  30s
```

