import http from 'k6/http';
import { check } from "k6";

export default function () {
    for (const url of [
        "https://sempolda.fr",
        "https://sempolda.fr/docs?topic=m%C3%A9dicaments%20g%C3%A9n%C3%A9riques",
        "https://sempolda.fr/api/topic-tree?root=%C3%A9conomie",
    ]) {
        let response = http.get(url);
        check(response, { "status was 200": (r) => r.status == 200 });
    }
}