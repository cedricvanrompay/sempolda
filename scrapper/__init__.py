from dataclasses import dataclass
import datetime
import json
import os
from pathlib import Path

@dataclass
class InternetRessource:
    url: str
    sha256: str

@dataclass
class Session:
    id: str
    date: datetime.date
    is_unfinished: bool
    path_to_official_html: str

    def xml_url(self):
        return f'https://www.assemblee-nationale.fr/dyn/opendata/{self.id}.xml'

def dump_json(data, path: str | Path):
    with open(path, 'w') as f:
        json.dump(data, f, indent=4, ensure_ascii=False)

class ScrapperException(Exception):
    """A base class for exceptions from the scrapper module."""

QUESTION_GOUV_ID_PREFIX = "assemblee-nationale/questions-gouv/"

