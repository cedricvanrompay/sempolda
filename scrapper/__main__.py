import argparse
import json
import sys
from datetime import datetime
from typing import Optional, Tuple
import os
import logging
import shutil
from hashlib import sha256
from pathlib import Path

from . import download, InternetRessource
from . import utils
from .scrap_xml import scrap_xml, export_question_gouv
from .list_sessions import yield_all_sessions_no_older_than
from .make_user_pick_dates import make_user_pick_dates, get_latest_dates_choice, Date
from .instance_query import get_dates_in_instance


logging.basicConfig(level=logging.INFO)


_SEMPOLDA_HOME = os.getenv('SEMPOLDA_HOME')

if not _SEMPOLDA_HOME:
    raise Exception("missing environment variable SEMPOLDA_HOME")

OUTPUT_ROOT = Path(_SEMPOLDA_HOME, 'scrapped')


def select_and_scrap(no_older_than: str, legislature: int, scrapped_time: datetime, db_instance: Optional[str] = None):
    sessions = list(yield_all_sessions_no_older_than(no_older_than, legislature))

    if not sessions:
        raise Exception("no sessions found")

    if db_instance:
        dates_in_db = get_dates_in_instance(db_instance)
    else:
        dates_in_db = None

    selected_dates = make_user_pick_dates(
        [
            Date(str(s.date), s.xml_url(), s.is_unfinished)
            for s in sessions
        ],
        db=dates_in_db,
    )

    scrap(selected_dates, scrapped_time)


def rescrap(path_to_scrapped: str, scrapped_time: datetime):
    p = Path(path_to_scrapped).expanduser()

    to_pick_from = list[Date]()

    for session in os.listdir(Path(p, "assemblee-nationale/questions-gouv")):
        file_path = Path(p, "assemblee-nationale/questions-gouv", session, "01/metadata.json")

        try:
            with open(file_path) as f:
                metadata = json.load(f)
        except FileNotFoundError:
            continue

        try:
            scrapped_from = metadata.get("scrapped_from")
            if not scrapped_from:
                print(f"""session {session}: No "scrapped_from" in 01/metadata.json""", file=sys.stderr)

            to_pick_from.append(Date(session, scrapped_from["url"], False))
        except Exception as err:
            print(f"session {session}: {err}")
            continue

    selected_dates = make_user_pick_dates(to_pick_from)
    
    scrap(selected_dates, scrapped_time)

def scrap_from_previous_selection(scrapped_time: datetime):
    selected_dates = get_latest_dates_choice()
    scrap(selected_dates, scrapped_time)

def scrap(selected_dates: list[Tuple[str, str]], scrapped_time: datetime):

    existing_content_directory = OUTPUT_ROOT/"assemblee-nationale/questions-gouv"

    try:
        existing_content = os.listdir(existing_content_directory)
    except FileNotFoundError:
        existing_content = None

    if existing_content:
        print(f"WARNING: existing content in {existing_content_directory}: {existing_content}")
        print("will be removed if continuing")
        input("hit Enter to continue or Ctrl-C to stop")

    os.makedirs(OUTPUT_ROOT, exist_ok=True)
    shutil.rmtree(OUTPUT_ROOT)
    os.mkdir(OUTPUT_ROOT)

    for nb, (date, url) in enumerate(selected_dates, start=1):
        logging.info(f'session {date}')

        response = download.get(url, with_cache=True)
        source = InternetRessource(url, sha256(response.content).hexdigest())

        session_dir = OUTPUT_ROOT/'assemblee-nationale/questions-gouv'/date

        debug_info = f"{date} ({nb}/{len(selected_dates)})"
        
        for q in scrap_xml(xml=response.text, debug_info=debug_info):
            q.scrapped_from = source
            export_question_gouv(q, datetime.date.fromisoformat(date), session_dir, scrapped_time)

        logging.info(f'written to {session_dir}')

if __name__ == '__main__':
    utils.ALLOW_DEBUGGER = True
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="cmd")

    parser_rescrap = subparsers.add_parser('rescrap')
    parser_rescrap.add_argument("scrapped")

    parser_scrap = subparsers.add_parser('scrap')
    parser_scrap.add_argument('--no-older-than', dest='no_older_than')
    parser_scrap.add_argument('--legislature', type=int)
    parser_scrap.add_argument('--reuse-selection', action='store_true')
    parser_scrap.add_argument("--instance", required=False)

    args = parser.parse_args()

    scrapped_time = datetime.now()

    if args.cmd == "scrap":
        if args.reuse_selection:
            scrap_from_previous_selection(scrapped_time)
        else:
            assert args.no_older_than
            assert args.legislature

            select_and_scrap(args.no_older_than, args.legislature, scrapped_time, args.instance)
    elif args.cmd == "rescrap":
        rescrap(args.scrapped, scrapped_time)