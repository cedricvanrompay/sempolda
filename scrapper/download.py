import os
import logging
import requests_cache

import requests

path_to_cache_db = os.path.expanduser('~/.local/share/sempolda/scrapper/cache')
os.makedirs(os.path.dirname(path_to_cache_db), exist_ok=True)

withCache = requests_cache.CachedSession(path_to_cache_db)

def get(url: str, with_cache=False):
    if with_cache:
        response = withCache.get(url)
    else:
        response = requests.get(url)

    response.raise_for_status()

    return response