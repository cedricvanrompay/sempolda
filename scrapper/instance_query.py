from dataclasses import dataclass
import requests

@dataclass
class InstanceDBInfo:
    domain: str
    scrapped_time_by_date: dict[str, str | None]

def get_dates_in_instance(address: str):
    response = requests.get(f"{address}/api/questions-gouv-sessions")
    response.raise_for_status()

    data = response.json()

    return InstanceDBInfo(
        domain=address,
        scrapped_time_by_date={
            date: session.get("scrapped_time")
            for date, session in data.items()
        }
    )

