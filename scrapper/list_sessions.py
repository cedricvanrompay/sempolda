import datetime
from urllib.parse import urlparse

import requests
import bs4

from . import Session
from . import utils

URL_PREFIX = 'https://www.assemblee-nationale.fr'
URL = f'{URL_PREFIX}/dyn/15/comptes-rendus/seance/'
PAGE_NB_WARNING = 25

def yield_all_sessions_no_older_than(min_date: str, legislature: int):
    page_nb = 1
    while True:
        url = f'{URL_PREFIX}/dyn/{legislature}/comptes-rendus/seance/'

        if page_nb > 1:
            url += f'?page={page_nb}'

        if page_nb == PAGE_NB_WARNING:
            print(f"WARNING: still listing pages at page number {page_nb}")
            print(f"maybe check the page manually: {url}")
            command = input((
                'enter "break" to stop the page listing here and continue, '
                'or just hit "enter" to continue listing pages, '
                'or "Ctrl-C" to exit'
            ))
            if command == "break":
                break
            elif command != "":
                raise Exception(f'unexpected command: "{command}"')

        response = requests.get(url)
        # Note: www.assemblee-nationale.fr will respond with "HTTP 200 OK"
        # even if there are no results,
        # for instance https://www.assemblee-nationale.fr/dyn/16/comptes-rendus/seance/?page=56
        # so this will not detect a "no results" page
        response.raise_for_status()

        soup = bs4.BeautifulSoup(response.text, "lxml")

        if any(
            'aucun' in tag.text.lower()
            for tag in soup.find_all('div', 'no-result')
        ):
            # no more pages for this "législature"
            # XXX fragile, causing is a risk of infinite loop
            # TODO find another way,
            # maybe look for several pages in a row with a small size?
            return

        for t in yield_sessions_transcripts_in_page(soup):
            if str(t.date) < min_date:
                return
            yield t

        page_nb += 1

def yield_sessions_transcripts_in_page(soup: bs4.BeautifulSoup):
    for ul_summary in soup.find_all('ul', 'crs-index-item-summary'):
        try:
            t = extract_transcript_info(ul_summary)
            if t:
                yield t
        except Exception as error:
            if utils.ALLOW_DEBUGGER:
                print(f'ERROR:', error)
                print('enter "continue" to skip')
                breakpoint()
                pass
            else:
                raise error from None
            pass

def extract_transcript_info(ul_summary: bs4.BeautifulSoup):
    try:
        sibling = ul_summary.find_next_sibling()
        session_id = sibling.attrs['id'].split('summary-')[1] #pyright: ignore
    except Exception as error:
        raise Exception((
            """unexpected next sibling of "ul.crs-index-item-summary": """
            f"{repr(error) = } {sibling.name =} {sibling.attrs}" #pyright: ignore
        ))

    for anchor in ul_summary.find_all('a', 'crs-sommaire-sommaire1'):
        if 'questions au gouvernement' not in anchor.text.lower():
            continue

        path_to_session_html = urlparse(anchor.attrs['href']).path
        date_slug = path_to_session_html.split('/')[-1].split('seance-du-')[1]
        date = parse_natural_french_date_slug(date_slug)

        is_unfinished = is_unfinished_transcript(anchor)

        return Session(session_id, date, is_unfinished, path_to_session_html)

weekdays = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']
months_ascii = [
    'janvier', 'fevrier', 'mars', 'avril',
    'mai', 'juin', 'juillet', 'aout',
    'septembre', 'octobre', 'novembre', 'decembre',
]
def parse_natural_french_date_slug(date_slug: str):
    weekday_name, day, month_name, year = date_slug.split("-")

    date = datetime.date(
        int(year),
        months_ascii.index(month_name) + 1,
        int(day),
    )

    assert date.weekday() == weekdays.index(weekday_name), f'"{date_slug}": {date} not a "{weekday_name}"'

    return date

def is_unfinished_transcript(anchor):
    session_container = anchor.parent.parent.parent
    assert session_container.name == 'div', f'expected "div" container, got "{session_container.name}"'

    if session_container.find('span', 'crs-index-item-provisoire'):
        return True
    else:
        return False

def merge_sessions_data(new: dict, old: dict):
    result = old.copy()

    for date, session in new.items():
        result[date] = session

    return
