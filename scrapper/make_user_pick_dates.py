"""DEPRECATED: use module "target_picker" instead"""
from dataclasses import dataclass
import datetime
from pathlib import Path
import subprocess
from typing import Optional, Tuple
import logging

from . import QUESTION_GOUV_ID_PREFIX
from .instance_query import InstanceDBInfo


FILE_PREFIX = 'sempolda-scraper-sessions-selection'


@dataclass
class Date:
    id: str
    url: str
    # replace with "comment: str"
    unfinished: bool


def _file_name(poldoc_type: str):
    parts = [
        FILE_PREFIX,
        poldoc_type.replace("/", "-"),
        datetime.datetime.now().isoformat(),
    ]

    return f"""/tmp/{"-".join(parts)}.txt"""


def make_user_pick_dates(sessions: list[Date], poldoc_type=QUESTION_GOUV_ID_PREFIX, db: Optional[InstanceDBInfo] = None):
    selection_file = _file_name(poldoc_type)

    with open(selection_file, 'w') as f:
        if db:
            f.write(f"# DB of instance at {db.domain}")
        else:
            f.write(f"# no info on dates in instance")

        f.write("\n\n")

        for s in sessions:
            date, url, is_unfinished = s
            line = f'{date} {url}'
            if is_unfinished:
                f.write(f'# {line} # not finished')
            elif db and date in db.scrapped_time_by_date:
                scrapped_time = db.scrapped_time_by_date.get(date)
                if scrapped_time:
                    f.write(f"{line} # scrapped on {scrapped_time}")
                else:
                    f.write(f"{line} # scrapped but no date")
            else:
                f.write(line)
            f.write('\n')

    subprocess.run(f'$EDITOR {selection_file}',shell=True)

    selected_dates = read_selected_dates(selection_file)
    if not selected_dates:
        raise Exception('no dates were picked')

    return selected_dates

def get_latest_dates_choice():
    try:
        path = max(Path('/tmp/').glob(FILE_PREFIX+'-*'))
    except ValueError:
        raise Exception('latest selection file not found')

    logging.info(f'using selection from {path}')
    return read_selected_dates(path)


def read_selected_dates(path_to_file) -> list[Tuple[str, str]]:
    result = list[Tuple[str, str]]()

    with open(path_to_file) as f:
        for line_nb, l in enumerate(f.readlines(), start=1):
            l = l.strip()

            if not l or l.startswith("#"):
                continue

            try:
                date, url = l.split("#")[0].strip().split()
                datetime.date.fromisoformat(date)
                assert url.startswith("https://")
                result.append((date, url))
            except Exception as error:
                error.add_note(f"line {line_nb}: "+l)
                raise error from None

    return result