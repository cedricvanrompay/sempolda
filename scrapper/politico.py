from dataclasses import dataclass
import dataclasses
from datetime import datetime, date, timezone
import json
import logging
import os
from pathlib import Path
import sys
from urllib.parse import urlparse

import requests
import bs4


from .target_picker import Target, make_user_pick_targets
from .utils import remove_existing_data

# will be removed when politico will be scrapped from main entry point
from .__main__ import OUTPUT_ROOT

OUTPUT_DIR = Path(OUTPUT_ROOT, "politico")


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat(timespec="seconds")
        elif isinstance(o, date):
            return o.isoformat()
        else:
            super().default(o)


@dataclass
class Metadata:
    """TODO use globally"""
    url: str
    title: str
    date: date
    scrapped_time: datetime


@dataclass
class Content:
    excerpt: str
    paragraphs: list[str]

@dataclass
class Article:
    metadata: Metadata
    content: Content


def _get_page_hrefs(page: int):
    args={
        "page": page,
        "post_types":["article","pro-free"],
        "tags":[105714,130923],
        "columns":4,
        "show_date":True,
        "show_primary_section":True,
        "posts_per_page":0,
    }

    response = requests.get(f"https://www.politico.eu/wp-json/politico/v1/content-listing?args={json.dumps(args)}")

    soup = bs4.BeautifulSoup(response.json()["html"], features="lxml")

    hrefs = list()

    for h2 in soup.find_all("h2", "card__title"):
        links = h2.find_all("a")

        if len(links) != 1:
            logging.warning(f"expected 1 link per h2, got {len(links)}; { h2 = }")

        hrefs.append(links[0].attrs["href"])

    return hrefs


def select(max_page: int):
    targets = list[Target]()

    for page in range(1, max_page+1):
        try:
            hrefs = _get_page_hrefs(page)

            for href in hrefs:
                targets.append(Target(href))
        except Exception as error:
            error.add_note(f"{ page = }")
            raise error from None
        
    selected = make_user_pick_targets(targets, poldoc_type="politico")

    return selected


def scrap(target: str):
    url = target
    response = requests.get(url)

    soup = bs4.BeautifulSoup(response.text, features="lxml")

    title = soup.find_all("h1", "hero__title")[0].text.strip()

    excerpt = soup.find_all("p", "hero__excerpt")[0].text.strip()

    paragraphs = [
        div.text.strip()
        for div in soup.find_all("div", "article__content")
    ]

    date = datetime.fromisoformat(
        soup.find_all("meta", property="article:published_time")[0]["content"]
    ).date()

    return Article(
        metadata=Metadata(
            url=url,
            title=title,
            date=date,
            scrapped_time=datetime.now(timezone.utc),
        ),
        content=Content(
            excerpt=excerpt,
            paragraphs=paragraphs,
        ),
    )


def write_article(article: Article):
    url_path = urlparse(article.metadata.url).path.strip("/")
    slug = os.path.split(url_path)[-1]

    if not slug:
        raise Exception("could not parse slug in URL: "+article.metadata.url)

    dir_path = Path(OUTPUT_DIR, slug)

    if dir_path.exists():
        raise Exception(f"directory exists: {dir_path}")

    os.mkdir(dir_path)

    for data, filename in [
        [article.content, "content.json"],
        [article.metadata, "metadata.json"],
    ]:
        path = Path(dir_path,filename)
        with open(path, "w") as f:
            json.dump(
                dataclasses.asdict(data),
                f,
                indent=4,
                ensure_ascii=False,
                cls=JSONEncoder,
            )
        logging.info(f"wrote {path}")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    remove_existing_data(OUTPUT_ROOT)

    selected = select(2)

    if not selected:
        sys.exit("nothing selected")

    if not OUTPUT_DIR.exists():
        os.mkdir(OUTPUT_DIR)

    for s in selected:
        try:
            scrapped = scrap(s)
            write_article(scrapped)
        except Exception as error:
            logging.error(f"{s}: {error}")