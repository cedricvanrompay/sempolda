from dataclasses import dataclass
import dataclasses
import json
import logging
import os
from pathlib import Path
from datetime import date, datetime
import sys
import bs4
import requests

from .utils import remove_existing_data
from .target_picker import Target, make_user_pick_targets

# will be removed when radiofrance will be scrapped from main entry point
from .__main__ import OUTPUT_ROOT

OUTPUT_DIR = Path(OUTPUT_ROOT, "radiofrance")


@dataclass
class Episode:
    official_source_url: str
    title: str
    date: date
    scrapped_time: datetime

    radio: str
    podcast: str
    id: str
    duration: str
    link: str
    concept_id: str

    paragraphs: list[str]

    def metadata(self):
        return {
            key: getattr(self, key)
            for key in Episode.__dataclass_fields__.keys()
            if key != "paragraphs"
        }
    
    def content(self):
        return {
            "paragraphs": self.paragraphs,
        }


@dataclass
class ListItem:
    radio: str
    podcast: str
    id: str
    duration: str
    title: str
    link: str
    concept_id: str


def select(radio: str, podcast: str, max_nb_page: int, start_page: int=1):
    targets = list[Target]()

    for page in range(start_page, start_page+max_nb_page):
        try:
            items = _get_one_page(radio, podcast, page)

            for item in items:
                targets.append(Target(item.link))
        except Exception as error:
            error.add_note(f"{page = }")
            raise error from None
    
    if not items:
        raise Exception("no episodes found")

    selected = make_user_pick_targets(targets, poldoc_type="radiofrance")

    return filter(
        lambda item: item.link in selected,
        items,
    )


def _get_one_page(radio: str, podcast: str, page: int):
    url = f"https://www.radiofrance.fr/{radio}/podcasts/{podcast}/__data.json"

    if page != 1:
        url += f"?p={page}"

    response = requests.get(url)
    response.raise_for_status()
    data = response.json()["nodes"][3]["data"]

    episodes = list[ListItem]()

    attributes_by_key = {
        key: key
        for key in ListItem.__dataclass_fields__.keys()
        if key not in [
            "concept_id", "radio", "podcast",
        ]
    }

    attributes_by_key["conceptId"] = "concept_id"

    for x in data:
        if type(x) != dict:
            continue

        try:
            episodes.append(ListItem(
                radio=radio,
                podcast=podcast,
                **{
                    attr: data[x[key]]
                    for key, attr in attributes_by_key.items()
                },
            ))
        except KeyError:
            continue

    return episodes


def scrap(item: ListItem):
    url = "https://radiofrance.fr"+item.link
    response = requests.get(url)

    soup = bs4.BeautifulSoup(response.text, features="lxml")

    paragraphs = [
        div.text.strip()
        for div in soup.find_all("p", "PopSlotParagraph")
    ]

    publication_info = soup.find("p", "CoverEpisode-publicationInfo")

    try:
        pub_date = parse_publication_date(publication_info.text) #pyright: ignore
    except Exception as error:
        error.add_note(f"{publication_info.text = }") #pyright: ignore
        raise error from None

    return Episode(
        official_source_url=url,
        date=pub_date,
        scrapped_time=datetime.now(),
        paragraphs=paragraphs,
        **dataclasses.asdict(item)
    )


def parse_publication_date(text: str):
    parts = text.split()[-4:]

    weekday = [
        "lundi", "mardi", "mercredi",
        "jeudi", "vendredi", "samedi", "dimanche",
    ].index(parts[0])
    day = int(parts[1])
    month = [
        "janvier", "février", "mars",
        "avril", "mai", "juin",
        "juillet", "août", "septembre",
        "octobre", "novembre", "décembre",
    ].index(parts[2]) + 1
    year = int(parts[3])

    if weekday == -1:
        raise Exception(f"could not parse weekday {parts[0]}")

    if month == 0:
        raise Exception("unexpected month")

    d = date(year, month, day)

    if d.weekday() != weekday:
        raise Exception(f"computed weekday {d.weekday()}, not {weekday}")
    
    return d


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat(timespec="seconds")
        elif isinstance(o, date):
            return o.isoformat()
        else:
            super().default(o)


def write_episode(episode: Episode):
    parts = episode.link.split("/")

    assert parts[:4] == ["", episode.radio, "podcasts", episode.podcast], f"{parts = }"

    slug = parts[4]

    dir_path = Path(OUTPUT_DIR, episode.radio, episode.podcast, slug)

    if dir_path.exists():
        raise Exception(f"directory exists: {dir_path}")

    os.makedirs(dir_path)

    for data, filename in [
        [episode.content(), "content.json"],
        [episode.metadata(), "metadata.json"],
    ]:
        path = Path(dir_path,filename)
        with open(path, "w") as f:
            json.dump(
                data,
                f,
                indent=4,
                ensure_ascii=False,
                cls=JSONEncoder,
            )
        logging.info(f"wrote {path}")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    remove_existing_data(OUTPUT_ROOT)

    selected = select(
        radio="franceculture",
        podcast="entendez-vous-l-eco",
        max_nb_page=1,
    )

    if not selected:
        sys.exit("nothing selected")

    for s in selected:
        try:
            scrapped = scrap(s)
            write_episode(scrapped)
        except Exception as error:
            log_msg = f"{s}: {error}"
            if error.__notes__:
                log_msg += "\n"+"\n".join(error.__notes__)

            logging.error(log_msg)
    