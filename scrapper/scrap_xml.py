import dataclasses
from datetime import datetime, date
import logging
import os
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import bs4

from . import ScrapperException, dump_json, InternetRessource
from . import utils

TYPE_QUESTION_OR_ANSWER = 'question_or_answer'
TYPE_ANSWER = "answer"
TYPE_PRESIDENT = 'president'
TYPE_INTERRUPTION = 'interruption'

@dataclass
class Orateur:
    name: str
    id_acteur: str
    status: str | None

@dataclass
class Intervention:
    paragraphs: list[str]
    type: str
    orateur: str | Orateur

@dataclass
class QuestionGouv:
    number: int
    title: str
    interventions: list[Intervention]
    scrapped_from: Optional[InternetRessource] = None

    def interventions_to_json(self):
        return [
            dataclasses.asdict(x)
            for x in self.interventions
        ]

def scrap_xml(xml: str, debug_info: str=""):
    soup = bs4.BeautifulSoup(xml, features='xml')

    points = soup.find_all('point', code_grammaire="QG_1_1")

    for nb, point in enumerate(points, start=1):
        try:
            if point.attrs['code_grammaire'].startswith('SUSP_SEAN'):
                # "suspensions de séance" are ignored
                continue

            question_debug_info = f"{debug_info}: points {nb}/{len(points)}"

            yield QuestionGouv(
                number=nb,
                title=point.texte.text,
                interventions=list(yield_interventions(point, question_debug_info))
            )
        except Exception as error:
            if utils.ALLOW_DEBUGGER:
                print(f'ERROR: question number {nb}:', error)
                print('enter "continue" to skip')
                breakpoint()
                pass
            else:
                raise Exception(f"processing question nb {nb}") from error
            pass

def get_intervention_type(paragraphe, debug_info: str=""):
    if paragraphe.attrs['code_grammaire'] == 'INTERRUPTION_1_10':
        return TYPE_INTERRUPTION 

    if paragraphe.attrs['code_grammaire'] != 'PAROLE_GENERIQUE':
        raise ScrapperException(f'''unknown "code_grammaire" value: "{paragraphe.attrs['code_grammaire']}"''')
    
    code_parole = paragraphe.attrs['code_parole']

    # see https://gitlab.com/cedricvanrompay/sempolda/-/issues/183 for the new codes
    if code_parole in ['PAROLE_1_1', "PRESIDE_DISCOURS_1_10", "AVIS_GVT_1_10"]:
        return TYPE_PRESIDENT

    # "AVIS_GVT_1_20" seems to be for answers from the government
    # but it's not clear if it's used consistently to we're using TYPE_QUESTION_OR_ANSWER
    if code_parole in ['PAROLE_1_2', "AVIS_GVT_1_20"]:
        return TYPE_QUESTION_OR_ANSWER

    # trying a new method of identification
    # see https://gitlab.com/cedricvanrompay/sempolda/-/merge_requests/173
    id_nomination_op = paragraphe.attrs["id_nomination_op"]
    if id_nomination_op.startswith("PM"):
        return TYPE_PRESIDENT
    elif id_nomination_op == "-1":
        return TYPE_QUESTION_OR_ANSWER

    if utils.ALLOW_DEBUGGER:
        manually_set_type = None
        print()
        print("cannot decide type of intervention.")
        if debug_info:
            print(debug_info)
        print("paragraphe:", paragraphe)
        print("you can manually return TYPE_INTERRUPTION, TYPE_PRESIDENT or TYPE_QUESTION_OR_ANSWER")
        breakpoint()
        if manually_set_type: return manually_set_type

    raise ScrapperException(f"paragraph did not match any type. attributes: {paragraphe.attrs!r}")

def yield_interventions(point: bs4.BeautifulSoup, debug_info: str=""):
    paragraphes = point.find_all('paragraphe')

    for nb, paragraphe in enumerate(paragraphes, start=1):
        paragraphe_debug_info = f"{debug_info}: paragraphe {nb}/{len(paragraphes)}"

        try:
            if paragraphe.attrs['code_grammaire'] in {"QG_1_50"}:
                assert paragraphe.attrs['roledebat'] == "president"
                # seems to be for the end of a session,
                # see scrapper/testdata/2023-12-19/18.json
                continue

            if paragraphe.attrs['code_grammaire'].startswith('SUSP_SEAN'):
                # "suspensions de séance" are ignored
                continue

            intervention_type = get_intervention_type(paragraphe, paragraphe_debug_info)
            if nb == 1 and intervention_type != TYPE_PRESIDENT:
                logging.warn(f'{debug_info}: first "paragraphe" was not labeled as "TYPE_PRESIDENT"')

            yield Intervention(
                paragraphs=list(yield_paragraphs(paragraphe)),
                orateur=get_orateur(paragraphe),
                type=intervention_type,
            )

        except Exception as error:
            if utils.ALLOW_DEBUGGER:
                print(f'ERROR:', error)
                print('enter "continue" to skip')
                breakpoint()
                pass
            else:
                raise error from None


def yield_paragraphs(paragraphe: bs4.BeautifulSoup):
    '''reads the "paragraphe" XML tag of the transcript
    and extracts the text, splitting it at the <br/> tags'''
    parts = list()

    for child in paragraphe.texte.children: # pyright: ignore
        if child.name == 'br': # pyright: ignore
            yield ' '.join(parts)
            parts = list()
        if child.name == 'italique': # pyright: ignore
            parts.append('<i>'+child.text.strip()+'</i>')
        elif not is_text_empty(child.text):
            parts.append(child.text.strip())

    if parts:
        yield ' '.join(parts)

def get_orateur(paragraphe):
    assert len(paragraphe.find_all('orateurs')) == 1

    orateur = paragraphe.orateurs.orateur

    assert orateur != None

    id_acteur = orateur.id.text

    if not id_acteur or id_acteur == "0":
        # happens for instance for interruptions attributed to several people
        return str(orateur.nom.text)

    p_attr = paragraphe.attrs.get("id_acteur")

    if p_attr != "PA" + id_acteur:
        # TODO add some context in the log; see https://gitlab.com/cedricvanrompay/sempolda/-/issues/214
        logging.warning(
            f"id acteur different from {p_attr = } and {orateur.id.text = }"
            f" (will use {id_acteur})"
        )

    # sometimes there can be mistakes in the XML
    name = orateur.nom.text.strip(", ")

    if name != orateur.nom.text:
        logging.warning(f"extra characters stripped from orateur \"{orateur.nom.text}\"")

    return Orateur(
        name=name,
        id_acteur=id_acteur,
        status=orateur.qualite.text or None,
    )


def export_question_gouv(q: QuestionGouv, date: date, session_dir: Path, scrapped_time: datetime):
    q_dir = session_dir/f'{q.number:02}'
    os.makedirs(q_dir)

    dump_json(q.interventions_to_json(), q_dir/'interventions.json')

    dump_json(
        {
            'title': q.title,
            "date": str(date),
            'scrapped_from': dataclasses.asdict(q.scrapped_from) if q.scrapped_from else None,
            # TODO put the version of the scrapper as well
            "scrapped_time": scrapped_time.isoformat(timespec="seconds"),
        },
        q_dir/'metadata.json',
    )

def is_text_empty(text: str) -> bool:
    for char in text:
        if char not in {
            '\n', ' ',
            '\xa0', # non-breaking space
            '\u2013', # em dash
        }:
            return False

    return True

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    args = parser.parse_args()

    with open(args.path) as f:
        xml = f.read()

    for question in scrap_xml(xml):
        print(dataclasses.asdict(question))