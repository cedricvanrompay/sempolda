from dataclasses import dataclass
import datetime
import os
import subprocess
from textwrap import indent


FILE_PREFIX = 'sempolda-scraper-sessions-selection'

@dataclass
class Target:
    data: str
    comment: str=""
    disabled: bool=False

def make_user_pick_targets(targets: list[Target], comment: str="", poldoc_type: str=""):
    editor = os.environ.get("EDITOR")

    if not editor:
        raise RuntimeError("no EDITOR in environment")

    selection_file = _file_name(poldoc_type)

    with open(selection_file, "w") as f:
        if comment:
            f.write(indent(comment, "# "))
            f.write("\n\n")

        for t in targets:
            line = t.data

            if t.comment:
                line += " # "+t.comment

            if t.disabled:
                line = "# "+line

            f.write(line+"\n")

    subprocess.run([editor, selection_file])

    with open(selection_file) as f:
        return _read_selected_targets(f.read())


def _file_name(poldoc_type: str | None):
    parts = [
        FILE_PREFIX,
    ]

    if poldoc_type:
        parts.append(poldoc_type)

    parts.append(datetime.datetime.now().isoformat())

    return f"""/tmp/{"-".join(parts)}.txt"""


def _read_selected_targets(text: str):
    selected = list[str]()

    enumerator = enumerate(text.splitlines(keepends=False), start=1)

    for line_nb, l in enumerator:
        if not l or l.startswith("#"):
            continue

        try:
            selected.append(l.split("#")[0].strip())
        except Exception as error:
            error.add_note(f"line {line_nb}: {l}")
            raise error from None

    return selected
