import glob
import json
import os
from pathlib import Path

import bs4

import scrapper
import scrapper.scrap_xml

def test_scrap_xml():
    test_data_dir = Path(scrapper.__file__).parent / 'testdata'

    for each in test_data_dir.iterdir():
        if not each.is_dir():
            continue

        with open(each/'source.xml') as f:
            xml = f.read()

        generator = scrapper.scrap_xml.scrap_xml(xml)

        numbers = sorted(map(
            lambda name: int(os.path.basename(os.path.splitext(name)[0])),
            glob.glob(str(each/'*.json')),
        ))

        for nb in range(1, max(numbers)+1):
            q = next(generator)
            assert q.number == nb

            path_to_json_file = each/f'{nb:02}.json'

            if not path_to_json_file.exists():
                continue

            with open(path_to_json_file) as f:
                expected = json.load(f)

            # # to update the test data
            # # note that it will stop after updating one file
            # try:
            #     assert q.interventions_to_json() == expected, f'unexpected result for question {each}/{nb:02}.json'
            # except Exception:
            #     with open(path_to_json_file, 'w') as f:
            #         json.dump(q.interventions_to_json(), f, indent=4, ensure_ascii=False)

            assert q.interventions_to_json() == expected, f'unexpected result for question {each}/{nb:02}.json'

def test_yield_paragraphs():
    p = bs4.BeautifulSoup('''
    <paragraphe>
        <texte stime="5862.31">
            Madame la députée, j’ai l’impression que vous connaissez mal l’Europe.
            <italique>(Rires et exclamations sur les bancs du groupe RN.)</italique>
        </texte>
    </paragraphe>
    ''', features='xml')

    assert list(scrapper.scrap_xml.yield_paragraphs(p)) == [
        "Madame la députée, j’ai l’impression que vous connaissez mal l’Europe. <i>(Rires et exclamations sur les bancs du groupe RN.)</i>"
    ]

if __name__ == '__main__':
    test_scrap_xml()

def update_expected():
    test_data_dir = Path(scrapper.__file__).parent / 'testdata'

    with open(test_data_dir/'CRSANR5L16S2022E1N004.xml') as f:
        xml = f.read()

    q = next(scrapper.scrap_xml.scrap_xml(xml))

    new = q.interventions_to_json()

    with open(test_data_dir/'assemblee-nationale%20questions-gouv%202022-07-12%2001%20interventions.json', 'w') as f:
        json.dump(new, f, indent=4, ensure_ascii=False)
