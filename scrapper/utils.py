import os
from pathlib import Path
import shutil


ALLOW_DEBUGGER = False


def remove_existing_data(output_root: Path):
    existing_data = dict()

    for doc_type in["assemblee-nationale/questions-gouv", "politico"]:
        try:
            entries = os.listdir(os.path.join(output_root, doc_type))

            if entries:
                existing_data[doc_type] = entries
        except FileNotFoundError:
            pass

    if existing_data:
        print(f"WARNING: existing content in {output_root} will be removed if continuing:")
        
        for doc_type, entries in existing_data.items():
            print(f"in {doc_type}:", ", ".join(entries))

        input("hit Enter to continue or Ctrl-C to stop")

    
    # creating parents if needed
    os.makedirs(output_root, exist_ok=True)

    shutil.rmtree(output_root)
    os.mkdir(output_root)

