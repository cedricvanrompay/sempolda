#!/usr/bin/env python3

import argparse
import datetime
import os
import subprocess
import sys
from typing import Tuple

import requests
import lib.config
from lib.utils import is_repo_dirty
from lib import build_compose_command
from lib.deployment import run_at_target
from lib.build import build_container_image

SERVICE = "auto_annotation_job"

PATH_TO_SELECTION_FILE = "/tmp/sempolda-auto-annotation-date-picking.txt"

def get_sessions_info_from_instance(address: str) -> list[Tuple[str, int, int]]:
    response = requests.get(f"{address}/api/questions-gouv-sessions")
    response.raise_for_status()

    sessions_by_date = response.json()

    
    return sorted(
        [
            [
                date,
                sum(q["annotated"] for q in session["questions"]),
                len(session["questions"]),
            ]
            for date, session in sessions_by_date.items()
        ],
        reverse=True,
    ) # pyright: ignore

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--to", choices=["local", "staging", "production"], required=True)
    parser.add_argument("sessions", nargs="*")
    args = parser.parse_args()

    if is_repo_dirty():
        input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

    if args.to == "production":
        print("WARNING: targeting production")
        if confirmation := input('type "production" to confirm: ') != "production":
            sys.exit(f'ERROR: expected "production", got "{confirmation}"')

    config = lib.config.get_config(staging=(args.to == "staging"))

    if len(args.sessions) > 0:
        cmd = ' '.join(args.sessions)
    else:
        if args.to == "local":
            sys.exit(
                "ERROR:"
                " listing sessions to annotate from instance"
                " is not implemented yet for local instance"
            )
        
        with open(PATH_TO_SELECTION_FILE, "w") as f:
            f.write(f"# data from {config.domain}\n")
            f.write("\n")

            for date, nb_annotated, nb_questions in get_sessions_info_from_instance(config.domain):
                row = f"{date} # {nb_annotated} / {nb_questions}\n"

                if nb_annotated > 0:
                    row = "# "+row

                f.write(row)

        subprocess.run(f"$EDITOR {PATH_TO_SELECTION_FILE}", shell=True)

        with open(PATH_TO_SELECTION_FILE) as f:
            dates = list()
            for line_nb, line in enumerate(f, start=1):
                line = line.strip()

                if not line or line.startswith("#"):
                    continue

                try:
                    date = line.split("#")[0].strip()
                    datetime.date.fromisoformat(date)
                    dates.append(date)
                except Exception as error:
                    print(f"ERROR parsing line {line_nb} ({line}): {error}")
            
        if not dates:
            sys.exit("no dates were picked")

        cmd = " ".join(dates)
        print("dates picked: "+", ".join(dates))
        input("press Ctrl-C to abort, or Return to continue")


    if args.to == "local":
        proc = subprocess.run(build_compose_command(
            SERVICE, cmd,
            prod_or_dev="dev",
            project_name=None,
            build=True,
        ).split(), check=True)

        sys.exit(proc.returncode)


    build_container_image("auto_annotation", "auto_annotation", config)
    
    cmd = build_compose_command(
        SERVICE, cmd,
        prod_or_dev="prod",
        project_name=config.compose_project_name,
        domain=config.domain,
        env_vars=[f"IMAGE_TAG={config.image_tag}"],
    )
    run_at_target([cmd], config)