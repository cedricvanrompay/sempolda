#!/usr/bin/env python3

import argparse
import datetime
import subprocess
import sys
import os.path

import lib.config
from lib.deployment import run_at_target


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--staging', action='store_true')
    parser.add_argument('--production', action='store_true')
    args = parser.parse_args()

    # TODO create a new Argument parser class
    # that handles the deployment target argument
    if not args.staging and not args.production:
        sys.exit("ERROR: either --staging or --production is required")
    if args.staging and args.production:
        sys.exit("ERROR: cannot have --staging and --production together")
    if args.production:
        print("WARNING: targeting production")
        if confirmation := input('type "production" to confirm: ') != "production":
            sys.exit(f'ERROR: expected "production", got "{confirmation}"')
    else:
        print("targeting staging")

    config = lib.config.get_config(args.staging)

    date = datetime.datetime.now().strftime("%FT%H-%M-%SZ")
    
    run_at_target(
        [
            f"mkdir -p /tmp/sempolda-backup/{date}",
            "docker build -t sempolda-backup ./backup",
            " ".join([
                "docker run",
                "-v sempolda_data:/var/lib/sempolda",
                "-v sempolda_auto-annotation-jobs-logs:/var/log/sempolda/auto-annotation-jobs",
                f"-v /tmp/sempolda-backup/{date}:/mnt/backup sempolda-backup:latest",
            ]),
        ],
        config,
    )

    subprocess.run(
        [
            "rsync",
            "--recursive",
            '--rsync-path=sudo rsync',
            f"{config.login_user}@{config.server}:/tmp/sempolda-backup/{date}",
            os.path.expanduser("~/sempolda/backups"),
        ],
        check=True,
    )

    print("backed up data to " + os.path.expanduser(f"~/sempolda/backups/{date}"))