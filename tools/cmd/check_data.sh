#!/usr/bin/bash
ssh root@ovhvps << 'EOF'
    docker run --rm \
        -v "sempolda-prod-data:/var/lib/sempolda" \
        -v "/root/repos/sempolda:/code" \
        -v "/root/sempolda:/mnt/host" \
        --workdir /code \
        python:3.10 \
        python check_data.py
EOF