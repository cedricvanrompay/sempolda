#!/usr/bin/bash
ssh root@ovhvps rm -rf /root/sempolda/scrapped/*

rsync --recursive --info=progress2 ~/.local/share/sempolda/scrapped/ root@ovhvps:/root/sempolda/scrapped/

ssh root@ovhvps << 'EOF'
    cd /root/repos/sempolda

    git status
    git pull

    docker run --rm \
        -v "sempolda_data:/var/lib/sempolda" \
        -v "/root/repos/sempolda:/code" \
        -v "/root/sempolda:/mnt/host" \
        --workdir /code \
        python:3.10 \
        python importer.py
EOF