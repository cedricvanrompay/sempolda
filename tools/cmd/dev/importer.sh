#!/usr/bin/env bash

# XXX this recreates a new container every time,
# taking more space and losing history.
# "docker start -i [name of previous container]" is better if container exists already;
# isn't there an equivalent for docker-compose?
docker-compose -f docker-compose.yml -f importer/docker-compose.yml run importer