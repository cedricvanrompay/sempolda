#!/usr/bin/bash
CMD_BASE="docker compose -f base.docker-compose.yml -f dev.docker-compose.yml"

$CMD_BASE config
$CMD_BASE up --build