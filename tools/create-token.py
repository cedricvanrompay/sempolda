#!/usr/bin/env python3

import argparse
import sys
import lib.config
from lib.deployment import run_at_target
from lib import build_compose_command

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--staging', action='store_true')
    parser.add_argument('--production', action='store_true')
    args = parser.parse_args()

    # TODO create a new Argument parser class
    # that handles the deployment target argument
    if not args.staging and not args.production:
        sys.exit("ERROR: either --staging or --production is required")
    if args.staging and args.production:
        sys.exit("ERROR: cannot have --staging and --production together")
    if args.production:
        print("WARNING: targeting production")
        if confirmation := input('type "production" to confirm: ') != "production":
            sys.exit(f'ERROR: expected "production", got "{confirmation}"')
    else:
        print("targeting staging")

    config = lib.config.get_config(args.staging)

    run_at_target(
        [
            build_compose_command(
                "admin",
                'create-token "Cédric Van Rompay"',
                # note: "prod" is used for staging as well,
                # the only other option is "dev"
                "prod",
                env_vars=[
                    f"IMAGE_TAG={config.image_tag}",
                ],
                project_name=config.compose_project_name,
                domain=config.domain,
                run_params=["--rm"],
            ),
        ],
        config,
    )
