set -o errexit
apt-get update
apt-get install -y curl jq less vim locales python3

echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen en_US.UTF-8