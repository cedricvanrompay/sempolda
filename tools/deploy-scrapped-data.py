#!/usr/bin/env python3

import argparse
import os.path
import subprocess
import sys
import lib.config
from lib.utils import is_repo_dirty
from lib import build_compose_command
from lib.deployment import run_at_target, copy_to_server
from lib.build import build_container_image 


SERVICE = "admin"
CMD = "import-scrapped-data"


def copy_scrapped_data_to_server(config: lib.config.Config):
    copy_to_server(
        os.path.expanduser('~/.local/share/sempolda/scrapped/'),
        "/root/sempolda/scrapped/",
        config,
    )


if __name__ == '__main__':
    # TODO create a new Argument parser class
    # that handles the deployment target argument
    parser = argparse.ArgumentParser()
    parser.add_argument("--to", choices=["local", "staging", "production"])
    parser.add_argument("--overwrite", action="store_true")
    args = parser.parse_args()

    cmd = CMD + (
        " --overwrite" if args.overwrite else ""
    )

    if args.to == "local":
        compose_cmd = build_compose_command(
            SERVICE, cmd,
            prod_or_dev="dev",
            project_name=None,
            build=True,
            run_params=(
                f"-v {os.path.expanduser('~/.local/share/sempolda/scrapped/')}:/mnt/scrapped-data"
            )
        )
        subprocess.run(compose_cmd.split(), check=True)
        sys.exit()

    if is_repo_dirty():
        input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

    if args.to == "staging":
        config = lib.config.get_config(staging=True)

        copy_scrapped_data_to_server(config)

        build_container_image("web-app", "web-app", config)

        compose_cmd = build_compose_command(
            SERVICE, cmd,
            prod_or_dev="prod",
            project_name=config.compose_project_name,
            domain=config.domain,
            env_vars=[f"IMAGE_TAG={config.image_tag}"],
            run_params=(
                f"-v /root/sempolda/scrapped:/mnt/scrapped-data"
            )
        )
        run_at_target([compose_cmd], config)
        
    elif args.to == "production":
        print("WARNING: targeting production")
        if confirmation := input('type "production" to confirm: ') != "production":
            sys.exit(f'ERROR: expected "production", got "{confirmation}"')

        config = lib.config.get_config(staging=False)

        copy_scrapped_data_to_server(config)

        build_container_image("web-app", "web-app", config)

        compose_cmd = build_compose_command(
            SERVICE, cmd,
            prod_or_dev="prod",
            project_name=config.compose_project_name,
            domain=config.domain,
            env_vars=[f"IMAGE_TAG={config.image_tag}"],
            run_params=(
                f"-v /root/sempolda/scrapped:/mnt/scrapped-data"
            )
        )
        run_at_target([compose_cmd], config)
