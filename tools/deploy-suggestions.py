#!/usr/bin/env python3

import argparse
import sys
import lib.config
from lib import build_compose_command
from lib.utils import is_repo_dirty
from lib.build import build_container_image
from lib.deployment import run_at_target

SERVICE = "annotation_suggestions"

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--to", choices=["staging", "production"], required=True)
    args = parser.parse_args()

    if is_repo_dirty():
        input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

    if args.to == "production":
        print("WARNING: targeting production")
        if confirmation := input('type "production" to confirm: ') != "production":
            sys.exit(f'ERROR: expected "production", got "{confirmation}"')
    else:
        print("targeting staging")

    config = lib.config.get_config(staging=(args.to == "staging"))

    build_container_image("auto_annotation", "auto_annotation", config)

    cmd = build_compose_command(
        SERVICE,
        cmd="",
        up_or_run="up",
        prod_or_dev="prod",
        project_name=config.compose_project_name,
        domain=config.domain,
        env_vars=[f"IMAGE_TAG={config.image_tag}"],
    )
    # TODO add "docker compose up healthcheck"
    run_at_target([cmd], config)
