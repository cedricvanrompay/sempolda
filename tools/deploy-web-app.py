#!/usr/bin/env python3

import argparse
import subprocess
import sys
import lib.config
from lib.utils import is_repo_dirty, get_current_branch
from lib.build import build_container_image

def deploy_web_app(branch: str, config: lib.config.Config):
    build_container_image("web-app", "web-app", config)

    print("TODO: use the new \"run_at_target\" function")
    subprocess.run(
        f'ssh {config.login_user}@{config.server}'.split(),
        text=True,
        input='\n'.join([
            'set -o errexit',
            'set -o verbose',
            "sudo su" if config.login_user != "root" else "",
            'cd /root/repos/sempolda',
            'git fetch',
            f'git checkout --force {branch}',
            f'git reset --hard origin/{branch}',
            'CURRENT_COMMIT=`git log -1 --format="%h"`',
            ' '.join([
                f'WEB_APP_PORT={config.web_app_port}',
                f'ORIGIN={config.domain}',
                f"IMAGE_TAG={config.image_tag}",
                'docker compose',
                f'-p {config.compose_project_name}',
                '--file base.docker-compose.yml',
                '--file prod.docker-compose.yml',
                "pull",
            ]),
            # quick fix
            # TODO fix in a cleaner way in https://gitlab.com/cedricvanrompay/sempolda/-/issues/223
            f"docker pull registry.gitlab.com/cedricvanrompay/sempolda/web-app:{config.image_tag}",
            ' '.join([
                f'WEB_APP_PORT={config.web_app_port}',
                f'ORIGIN={config.domain}',
                f"IMAGE_TAG={config.image_tag}",
                'docker compose',
                f'-p {config.compose_project_name}',
                '--file base.docker-compose.yml',
                '--file prod.docker-compose.yml',
                # note: we need "up" and not "run"
                # because only "up" will stop any existing instance
                # before starting the new one
                'up',
                '--force-recreate',
                '--detach',
                'web-app',
            ]),
        ])
    )

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--staging', action='store_true')
    parser.add_argument('--production', action='store_true')
    args = parser.parse_args()

    # TODO create a new Argument parser class
    # that handles the deployment target argument
    if not args.staging and not args.production:
        sys.exit("ERROR: either --staging or --production is required")
    if args.staging and args.production:
        sys.exit("ERROR: cannot have --staging and --production together")
    if args.production:
        print("WARNING: targeting production")
        if confirmation := input('type "production" to confirm: ') != "production":
            sys.exit(f'ERROR: expected "production", got "{confirmation}"')
    else:
        print("targeting staging")

    config = lib.config.get_config(args.staging)

    if is_repo_dirty():
        input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

    branch = get_current_branch()

    deploy_web_app(branch, config)

