import subprocess
from .utils import get_current_branch


def build_compose_command(
        service: str,
        cmd: str, # TODO make optional
        prod_or_dev: str,
        project_name: str | None,
        up_or_run: str = "run",
        env_vars: list[str] | None=None,
        build: bool=False,
        domain: str | None=None,
        run_params: str | list[str] | None=None
    ):

    if prod_or_dev not in {"prod", "dev"}:
        raise ValueError(f"unexpected value: {prod_or_dev = }")

    if up_or_run not in {"up", "run"}:
        raise ValueError(f"unexpected value: {up_or_run = }")

    parts = list()

    if domain:
        parts.append(f"DOMAIN={domain}")

    if env_vars:
        parts.extend(env_vars)

    parts.append("docker compose")

    if project_name:
        parts.append(f"-p {project_name}")

    parts.append("--file base.docker-compose.yml")

    parts.append(f"--file {prod_or_dev}.docker-compose.yml")

    parts.append(up_or_run)

    if up_or_run == "up":
        parts.append("--detach")

    if build:
        parts.append("--build")

    if run_params:
        if type(run_params) == str:
            parts.append(run_params)
        elif type(run_params) == list:
            parts.extend(run_params)
        else:
            raise TypeError(f"{type(run_params) = }")

    parts.append(service)
    parts.append(cmd)

    return " ".join(parts)


def run_via_ssh(cmd: str, server: str):
    print("TODO: use the new \"run_at_target\" function instead")
    branch = get_current_branch()

    subprocess.run(
        ["ssh", f"root@{server}"],
        check=True,
        text=True,
        input='\n'.join([
            'set -o errexit',
            'set -o verbose',
            'cd repos/sempolda',
            'git fetch',
            f'git checkout --force {branch}',
            f'git reset --hard origin/{branch}',
            cmd,
        ])
    )
