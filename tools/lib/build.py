import os
import subprocess
from lib.config import Config


def build_container_image(context_dir: str, image_name:str, config: Config):
    assert os.path.isdir(context_dir), context_dir

    image_ref = f"registry.gitlab.com/cedricvanrompay/sempolda/{image_name}:{config.image_tag}"

    subprocess.run(
        f"docker build -t {image_ref} {context_dir}/".split(),
        check=True,
    )

    subprocess.run(
        f"docker push {image_ref}".split(),
        check=True,
    )