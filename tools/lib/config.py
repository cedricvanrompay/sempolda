from dataclasses import dataclass

@dataclass
class Config:
    # XXX it's weird to call this "domain"
    # when it's a full address not just a domain
    domain: str
    server: str
    web_app_port: int
    compose_project_name: str
    login_user: str
    image_tag: str

PRODUCTION = Config(
    domain="https://sempolda.fr",
    server="sempolda.fr",
    web_app_port=3030,
    compose_project_name="sempolda",
    login_user="ubuntu",
    image_tag="prod",
)

STAGING = Config(
    domain="https://staging.sempolda.fr",
    server="staging.sempolda.fr",
    web_app_port=3031,
    compose_project_name="sempolda_staging",
    login_user="ubuntu",
    image_tag="staging",
)

def get_config(staging: bool):
    if staging:
        return STAGING

    return PRODUCTION