import subprocess
from pathlib import Path

from .config import Config
from .utils import get_current_branch, is_repo_dirty

def run_at_target(instructions: list[str], config: Config):
    if is_repo_dirty():
        input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

    branch = get_current_branch()

    subprocess.run(
        f'ssh {config.login_user}@{config.server}'.split(),
        text=True,
        input='\n'.join([
            'set -o errexit',
            'set -o verbose',
            "sudo su" if config.login_user != "root" else "",
            'cd /root/repos/sempolda',
            'git fetch',
            f'git checkout --force {branch}',
            f'git reset --hard origin/{branch}',
            'CURRENT_COMMIT=`git log -1 --format="%h"`',
            *instructions,
        ]),
    )


def copy_to_server(src: str | Path, dest: str, config: Config):
    if not dest.startswith("/root/sempolda/"):
        raise Exception(f"""destination does not start with "/root/sempolda/": {dest}""")
        
    subprocess.run(
        [
            'rsync',
            '--recursive',
            '--delete',
            '--rsync-path=sudo rsync',
            src,
            f"{config.login_user}@{config.server}:{dest}",
        ],
        check=True,
        capture_output=True,
    )