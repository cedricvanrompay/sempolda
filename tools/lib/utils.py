import re
import subprocess


def is_repo_dirty():
    proc = subprocess.run(
        'git status --short'.split(),
        capture_output=True,
        check=True,
    )
    out = proc.stdout

    if len(out) > 0:
        return True
    else:
        return False

BRANCH_HEAD_REGEXP = re.compile(r'^ref: refs/heads/(?P<refname>[a-zA-Z0-9-/]+)$')

def get_current_branch():
    with open('.git/HEAD') as f:
        head = f.read().strip()

    match = BRANCH_HEAD_REGEXP.match(head)
    
    if not match:
        raise Exception(f'git head does not point to a branch: {head}')

    branch = match.group('refname')

    return branch