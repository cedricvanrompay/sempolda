#!/usr/bin/env python3

import argparse
import subprocess
import sys
import lib.config
from lib.utils import is_repo_dirty
from lib import build_compose_command
from lib.build import build_container_image
from lib.deployment import run_at_target

# XXX this service is made for a server, with healthcheck and all;
# maybe use the "auto_annotation_job" service instead?
SERVICE = "annotation_suggestions"
CMD = "pipenv run python -m auto_annotation.recompute_tfidf"

if __name__ == '__main__':
    # TODO create a new Argument parser class
    # that handles the deployment target argument
    parser = argparse.ArgumentParser()
    parser.add_argument("--to", choices=["local", "staging", "production"], required=True)
    args = parser.parse_args()

    if args.to == "local":
        subprocess.run(build_compose_command(
            SERVICE, CMD,
            prod_or_dev="dev",
            project_name=None,
            build=True,
        ).split(), check=True)
        sys.exit()

    if is_repo_dirty():
        input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

    if args.to == "production":
        print("WARNING: targeting production")
        if confirmation := input('type "production" to confirm: ') != "production":
            sys.exit(f'ERROR: expected "production", got "{confirmation}"')

    config = lib.config.get_config(staging=(args.to == "staging"))

    build_container_image("auto_annotation", "auto_annotation", config)
    
    cmd = build_compose_command(
        SERVICE, CMD,
        prod_or_dev="prod",
        project_name=config.compose_project_name,
        domain=config.domain,
        env_vars=[f"IMAGE_TAG={config.image_tag}"],
    )
    run_at_target([cmd], config)
