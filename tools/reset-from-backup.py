#!/usr/bin/env python3
import argparse
from pathlib import Path
import subprocess
import sys

from lib import build_compose_command
from lib.config import Config, get_config
from lib.deployment import copy_to_server, run_at_target
from lib.utils import is_repo_dirty

SERVER_BACKUPS_DIR = "/root/sempolda/backups/"

def copy_backup(backup: Path, config: Config):
    copy_to_server(
        backup,
        SERVER_BACKUPS_DIR,
        config,
    )


SERVICE = "admin"
CMD = "reset-from-backup"


parser = argparse.ArgumentParser()
parser.add_argument("--to", choices=["local", "staging", "production"])
parser.add_argument("--delete-existing-scrapped-data", action="store_true")
parser.add_argument("path_to_backup")
args = parser.parse_args()

path_to_backup = Path(args.path_to_backup)
expected_parent = Path("~/sempolda/backups").expanduser()
if not path_to_backup.is_relative_to(expected_parent):
    raise Exception(f"path is not under {expected_parent}: {path_to_backup}")

backup_dirname = path_to_backup.name

cmd = CMD+" /mnt/backup"

# TODO propagate this automatically
if args.delete_existing_scrapped_data:
    cmd += " --delete-existing-scrapped-data"

if args.to == "local":
    compose_cmd = build_compose_command(
        SERVICE, cmd,
        prod_or_dev="dev",
        project_name=None,
        build=True,
        run_params=(
            f"-v {path_to_backup}:/mnt/backup"
        )
    )
    subprocess.run(compose_cmd.split(), check=True)
    sys.exit()

if is_repo_dirty():
    input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

backup_dir_on_server = Path(SERVER_BACKUPS_DIR, backup_dirname)

if args.to == "staging":
    config = get_config(staging=True)

    copy_backup(path_to_backup, config)

    compose_cmd = build_compose_command(
        SERVICE, cmd,
        prod_or_dev="prod",
        project_name=config.compose_project_name,
        domain=config.domain,
        env_vars=[f"IMAGE_TAG={config.image_tag}"],
        run_params=(
            f"-v {backup_dir_on_server}:/mnt/backup"
        )
    )
    run_at_target([compose_cmd], config)
    
elif args.to == "production":
    print("WARNING: targeting production")
    if confirmation := input('type "production" to confirm: ') != "production":
        sys.exit(f'ERROR: expected "production", got "{confirmation}"')

    config = get_config(staging=False)

    copy_backup(path_to_backup, config)

    compose_cmd = build_compose_command(
        SERVICE, cmd,
        prod_or_dev="prod",
        project_name=config.compose_project_name,
        domain=config.domain,
        env_vars=[f"IMAGE_TAG={config.image_tag}"],
        run_params=(
            f"-v {backup_dir_on_server}:/mnt/backup"
        )
    )
    run_at_target([compose_cmd], config)


