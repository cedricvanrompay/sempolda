#!/usr/bin/env python3

import argparse
import subprocess
import sys

import lib.config


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--to", choices=["local", "staging", "production"], required=False)
    args = parser.parse_args(
        sys.argv[1:sys.argv.index("-")] if "-" in sys.argv
        else sys.argv[1:]
    )

    rest = sys.argv[sys.argv.index("-")+1:] if "-" in sys.argv else []

    if args.to == "local":
        instance = "http://localhost:5173"
    elif args.to:
        config = lib.config.get_config(staging=(args.to == "staging"))
        instance = config.domain
    else:
        instance = None

    # we execute the scrapper as a separate program instead of doing a Python import
    # because the scrapper runs in its own python virtual env (via pipenv)
    proc = subprocess.run(" ".join([
        "PIPENV_PIPFILE=scrapper/Pipfile pipenv run python -m scrapper scrap",
        f"--instance {instance}" if instance else "",
        *rest,
    ]), shell=True)

    sys.exit(proc.returncode)