import typescript from '@rollup/plugin-typescript';

export default {
	input: 'src/admin/index.ts',
	output: {
		file: 'build/admin.js',
		format: 'es'
	},
    plugins: [typescript()],
};