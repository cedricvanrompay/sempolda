import Database from "better-sqlite3";

import * as database from "$lib/database";
import { labelExists } from "./utils";

export function addSynonym(synonym: string, label: string) {
    const db = new Database(database.PATH, {fileMustExist: true});

    if (labelExists(synonym, db)) {
        throw Error(`"${synonym}" already exists as a topic`);
    }

    try {
        db.prepare(`INSERT INTO synonyms(name, label_name)
            VALUES (?, ?)
        `).run(synonym, label);
    } catch (error) {
        // @ts-ignore
        if (error.code == "SQLITE_CONSTRAINT_FOREIGNKEY") {
            throw Error(`label "${label}" does not exist (${error})`)
        }

        throw error;
    }

    console.log(`added "${synonym}" as a synonym of "${label}"`)
}
