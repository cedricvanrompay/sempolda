import fs from "node:fs";
import path from "node:path";

import * as database from "$lib/database";
import type { Metadata } from "$lib/metadata";
import { importQuestionGouvs } from "./questionsGouv";


export function importScrappedData(pathToSourceDirectory: string, overwrite: boolean) {
    const process = new ImportProcess(pathToSourceDirectory, overwrite);
    process.import();
}


class ImportProcess {
    pathToSourceDirectory: string
    overwrite: boolean
    db: database.MainDb

    constructor(pathToSourceDirectory: string, overwrite: boolean) {
        this.pathToSourceDirectory = pathToSourceDirectory;
        this.overwrite = overwrite;

        this.db = new database.MainDb(database.PATH, { fileMustExist: true });
    }

    async import() {
        await importQuestionGouvs({
            pathToSourceDirectory: this.pathToSourceDirectory,
            overwrite: this.overwrite,
            db: this.db,
            logContext: "importing question gouvs",
        })

        for (const type of [
            "politico",
            "radiofrance/franceculture/entendez-vous-l-eco",
        ]) {
            this.importAllofType(type, `importing all of type ${type}`)
        }
    }

    private importAllofType(type: string, logContext: string) {
        const dir = path.join(this.pathToSourceDirectory, type);

        if (!fs.statSync(dir, { throwIfNoEntry: false })) {
            return;
        }

        for (const entry of fs.readdirSync(dir, { withFileTypes: true })) {
            if (!entry.isDirectory()) {
                console.warn(`${logContext}: ignoring non-directory entry in "${dir}": ${entry.name}`);
                continue;
            }

            const poldocId = path.join(type, entry.name);

            const ctx = `${logContext}: importing ${poldocId}`;

            this.importOnePoldoc({
                poldocId,
                logContext: ctx,
            })

            // weird: does not work, you get the context but not the error,
            // and you get a UnhandledPromiseRejection.
            // try {
            //     this.importOnePoldoc({
            //         poldocId,
            //         logContext: `${logContext}: importing ${poldocId}`,
            //     })
            // } catch (error) {
            //     throw prependContextToError(error as any, `${logContext}: importing ${poldocId}`);
            // }
        }
    }

    private importOnePoldoc({
        poldocId,
        logContext,
    }: {
        poldocId: string,
        logContext: string,
    }) {
        const pathToDestinationDir = path.join(database.SCRAPPED_DATA_PATH, poldocId);

        if (!this.overwrite && fs.existsSync(pathToDestinationDir)) {
            throw new Error(`not in "overwrite" mode and destination directory already exists: ${pathToDestinationDir}`)
        }

        // we cannot use getMetadata from metadata.ts
        // because the file we read is the one we are importing,
        // it's not a the place expected by getMetadata
        // TODO read source URL in metadata
        const metadata: Metadata = JSON.parse(fs.readFileSync(
            path.join(this.pathToSourceDirectory, poldocId, 'metadata.json'),
            { encoding: "utf-8" },
        ));

        if (!metadata.date) {
            throw Error(`${logContext}: missing date in metadata`);
        }

        if (this.overwrite) {
            // should we instead delete the existing row
            // and then insert the new one with the same logic as with overwrite=false?
            // this would allow us to move all of the "overwrite" logic
            // to a dedicated function
            try {
                // see https://sqlite.org/lang_upsert.html
                const result = this.db.connection.prepare(`
                INSERT INTO docs (id, title, date, official_source_url) VALUES (?, ?, ?, ?)
                    ON CONFLICT(id) DO UPDATE
                    SET title=excluded.title, official_source_url=excluded.official_source_url, date=excluded.date
            `).run(poldocId, metadata.title, metadata.date, metadata.official_source_url);

                // with such an upsert we cannot know if the row was already existing or not
                // because result.changes will be 1 whether the row was inserted or updated

                if (result.changes != 1) {
                    console.warn(`${logContext}: importing ${poldocId}: unexpected number of DB rows upserted: ${result.changes}`)
                }
            } catch (error) {
                console.warn(`${logContext}: importing ${poldocId}: upserting "docs" row: ${error}`)
            }
        } else {
            try {
                this.db.connection.prepare('INSERT INTO docs (id, title, date, official_source_url) VALUES (?, ?, ?, ?)')
                    .run(poldocId, metadata.title, metadata.date, metadata.official_source_url);
            } catch (error) {
                // @ts-ignore
                // note the error code is not "SQLITE_CONSTRAINT_UNIQUE"
                // even though the error message is "UNIQUE constraint failed"
                if (error.code == "SQLITE_CONSTRAINT_PRIMARYKEY") {
                    console.warn(`${logContext}: importing ${poldocId}: inserting row in database: ID was already present`);
                } else {
                    throw error;
                }
            }
        }


        if (this.overwrite) {
            try {
                fs.rmSync(pathToDestinationDir, { recursive: true })
            } catch (error) {
                console.warn(`importing ${poldocId}: removing ${pathToDestinationDir} for overwrite: ${error}`)
            }
        }

        // note that with "recursive=true",
        // this function will not return an error if the directory exists already
        // (unlike with "recursive=false")
        fs.mkdirSync(pathToDestinationDir, { recursive: true });

        for (const filename of ["metadata.json", "content.json"]) {
            try {
                fs.copyFileSync(
                    path.join(this.pathToSourceDirectory, poldocId, filename),
                    path.join(pathToDestinationDir, filename),
                    fs.constants.COPYFILE_EXCL,
                )
            } catch (error) {
                console.warn(`${logContext}: could not copy ${filename}: ${error}`)
            }
        }

        console.log(`${logContext}: success`);
    }
}

