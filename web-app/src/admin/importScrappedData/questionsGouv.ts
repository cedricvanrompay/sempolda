import fs from "node:fs";
import path from "node:path";
import { randomBytes } from 'node:crypto';

import * as database from "$lib/database";
import { Autocompleter } from "$lib/autocompletion";
import { getInterventions } from "$lib/questionsGouv";
import { POLDOC_TYPE_PREFIXES, type Intervention, type OrateurDetails, type Person } from "$lib/types";
import type { Metadata } from "$lib/metadata";
import { prependContextToError } from "../utils";


export async function importQuestionGouvs({
    pathToSourceDirectory,
    overwrite,
    db,
    logContext,
}: {
    pathToSourceDirectory: string
    overwrite: boolean,
    db: database.MainDb,
    logContext: string,
}) {
    const personAutocompleter = new Autocompleter<Person>();
    for (const person of db.connection.prepare(`SELECT * FROM persons`).all() as Person[]) {
        personAutocompleter.add(person.name, person);
    }

    const pathToTypeDirectory = path.join(pathToSourceDirectory, POLDOC_TYPE_PREFIXES.questionGouv);
    try {
        fs.statSync(pathToTypeDirectory)
    } catch (error) {
        // @ts-ignore
        if (error.code == "ENOENT") {
            console.log(`${logContext}: not found: ${pathToTypeDirectory}`);
            return
        }
    }

    for (const entry of fs.readdirSync(pathToTypeDirectory, {withFileTypes: true})){
        if (!entry.isDirectory()) {
            console.warn(`ignoring non-directory entry in "${pathToTypeDirectory}": ${entry.name}`);
            continue;
        }

        const date = entry.name;

        const pathToSessionDirectory = path.join(pathToTypeDirectory, date);
        for (const entry of fs.readdirSync(pathToSessionDirectory, {withFileTypes:true})) {
            if (!entry.isDirectory()) {
                console.warn(`ignoring non-directory entry in "${pathToSessionDirectory}": ${entry.name}`);
                continue;
            }

            const questionNb = entry.name;
            const poldocId = path.join(POLDOC_TYPE_PREFIXES.questionGouv, date, questionNb);

            const ctx = `${logContext}: importing ${poldocId}`;

            try {
                await importOneQuestion({
                    questionNb, poldocId, pathToSessionDirectory, overwrite,
                    db, personAutocompleter, logContext: ctx,
                });
            } catch (error) {
                if (error instanceof Error) {
                    error.message = `${ctx}: ${error.message}`
                }
                console.error(error);
                continue;
            }

        }
    }
}


/**
 * TODO use importOnePoldoc
 * plus a bit of custom logic
 */
async function importOneQuestion({
    questionNb,
    poldocId,
    pathToSessionDirectory,
    overwrite,
    db,
    personAutocompleter,
    logContext,
}: {
    questionNb: string,
    poldocId: string,
    pathToSessionDirectory: string,
    overwrite: Boolean,
    db: database.MainDb,
    personAutocompleter: Autocompleter<Person>,
    logContext: string,
}) {
    const pathToPoldoc = path.join(pathToSessionDirectory, questionNb);
    const pathToDestinationDir = path.join(database.SCRAPPED_DATA_PATH, poldocId);

    if (!overwrite && fs.existsSync(pathToDestinationDir)) {
        throw new Error(`not in "overwrite" mode and destination directory already exists: ${pathToDestinationDir}`)
    }

    // we cannot use getMetadata from metadata.ts
    // because the file we read is the one we are importing,
    // it's not a the place expected by getMetadata
    const metadata: Metadata = JSON.parse(fs.readFileSync(
        path.join(pathToPoldoc, 'metadata.json'),
        { encoding: "utf-8" },
    ));

    if (!metadata.date) {
        throw Error(`${logContext}: no date in metadata`)
    }

    // TODO insert additional metadata like link to source on official website

    if (overwrite) {
        try {
            // see https://sqlite.org/lang_upsert.html
            const result = db.connection.prepare(`
                INSERT INTO docs (id, title) VALUES (?, ?)
                    ON CONFLICT(id) DO UPDATE SET title=excluded.title
            `).run(poldocId, metadata.title);

            // with such an upsert we cannot know if the row was already existing or not
            // because result.changes will be 1 whether the row was inserted or updated

            if (result.changes != 1) {
                console.warn(`importing ${poldocId}: unexpected number of DB rows upserted: ${result.changes}`)
            }
        } catch (error) {
            console.warn(`importing ${poldocId}: upserting "docs" row: ${error}`)
        }
    } else {
        try {
            db.connection.prepare('INSERT INTO docs (id, title) VALUES (?, ?)')
                .run(poldocId, metadata.title);
        } catch (error) {
            // @ts-ignore
            // note the error code is not "SQLITE_CONSTRAINT_UNIQUE"
            // even though the error message is "UNIQUE constraint failed"
            if (error.code == "SQLITE_CONSTRAINT_PRIMARYKEY") {
                console.warn(`importing ${poldocId}: inserting row in database: ID was already present`);
            } else {
                throw error;
            }
        }
    }

    if (overwrite) {
        try {
            fs.rmSync(pathToDestinationDir, {recursive: true})
        } catch (error) {
            console.warn(`importing ${poldocId}: removing ${pathToDestinationDir} for overwrite: ${error}`)
        }
    }

    // note that with "recursive=true",
    // this function will not return an error if the directory exists already
    // (unlike with "recursive=false")
    fs.mkdirSync(pathToDestinationDir, {recursive: true});

    fs.copyFileSync(
        path.join(pathToPoldoc, 'interventions.json'),
        path.join(pathToDestinationDir, 'interventions.json'),
        fs.constants.COPYFILE_EXCL,
    )

    try {
        fs.copyFileSync(
            path.join(pathToPoldoc, 'metadata.json'),
            path.join(pathToDestinationDir, 'metadata.json'),
            fs.constants.COPYFILE_EXCL,
        )
    } catch (error) {
        console.warn(`importing ${poldocId}: could not copy metadata.json: ${error}`)
    }


    const interventions = await getInterventions(poldocId);
    setQuestionPeople({
        poldocId, interventions, db, overwrite, personAutocompleter,
        logContext: `${logContext}: setting questions' people`
    })

    console.log(`imported ${poldocId}`);
}


function setQuestionPeople({
    poldocId,
    interventions,
    db,
    personAutocompleter,
    overwrite,
    logContext,
}: {
    poldocId: string,
    interventions: Intervention[],
    db: database.MainDb,
    personAutocompleter: Autocompleter<Person>,
    overwrite: Boolean,
    logContext: string,
}) {
    if (overwrite) {
        try {
            db.connection.prepare(`
                DELETE FROM docs_persons
                WHERE doc_id = ?
            `).run(poldocId);

            // we don't try to delete the corresponding persons
            // because they may still be linked to other documents;
            // we can still search for persons linked to no documents
            // later in the process
        } catch (error) {
            if (error instanceof Error) {
                error.message = `${logContext}: deleting existing doc persons: ${error.message}`;
            }

            throw error;
        }
    }

    const mainActors = getMainActors( interventions, `${logContext}: getting main actors`);

    for (const actor of mainActors) {
        setOneQuestionOrateur({
            poldocId,
            orateur: actor,
            db,
            personAutocompleter,
            logContext: `${logContext}: actor ${JSON.stringify(actor)}`,
        })
    }
}


function setOneQuestionOrateur({
    poldocId,
    orateur,
    db,
    personAutocompleter,
    logContext,
}: {
    poldocId: string,
    orateur: OrateurDetails,
    db: database.MainDb,
    personAutocompleter: Autocompleter<Person>,
    logContext: string,
}) {
    let orateurAsPerson = db.getAssembleeNationaleActor(orateur.id_acteur);

    if (!orateurAsPerson) {
        // XXX the person could already be in the "person" table
        // but not yet in the "assemble_nationale_acteurs_persons" table,
        // even if it's not very likely
        // as long as we only process documents from the assemblée nationale

        // we only take the candidate with the highest score
        
        const autocompletion = personAutocompleter.complete(orateur.name);

        let candidate: Person | undefined = undefined;
        let score: number | undefined = undefined;

        if (autocompletion.length > 0) {
            candidate = autocompletion[0].value;
            score = autocompletion[0].score;
        }

        let personCreatedOrFound: string;

        if (candidate && score && score > 85) {
            orateurAsPerson = db.getPersonbyName(candidate.name);

            // TODO check that this person is not already linked to a row in assemble_nationale_acteurs_persons
            // (insertAssembleeNationaleActor will fail in this case anyway)

            if (!orateurAsPerson) {
                throw Error(`${logContext}: could not find person "${candidate}" in DB despite it being returned by autocompleter`)
            }

            personCreatedOrFound = "found in DB";
        } else {
            orateurAsPerson = {
                id: randomBytes(16).toString("hex"),
                name: orateur.name,
            }

            console.log(`${logContext}: inserting new person in DB: ${orateur.name}`)
            db.insertPerson(orateurAsPerson);
            personAutocompleter.add(orateur.name, orateurAsPerson);

            personCreatedOrFound = "created";
        }

        try {
            db.insertAssembleeNationaleActor(orateur.id_acteur, orateurAsPerson);
        } catch (error) {
            error = prependContextToError(
                [
                    "linking person",
                    JSON.stringify(orateurAsPerson),
                    `(${personCreatedOrFound})`,
                    "to assemblée acteur",
                    JSON.stringify(orateur),
                ].join(" "),
                error,
            )

            throw error;
        }
    }

    db.insertDocPerson(poldocId, orateurAsPerson.id);
}


function getMainActors(interventions: Intervention[], logContext: string = "") {
    const detailsByID = new Map<string, OrateurDetails>();

    for (const intervention of interventions) {
        if (
            !intervention.type
            || intervention.type == "interruption"
            || intervention.type == "president"
        ) {
            continue;
        }

        if (intervention.type != "question_or_answer") {
            console.warn(`${logContext}: unknown intervention type: "${intervention.type}"`);
            continue;
        }

        if (!intervention.orateur) {
            continue;
        }

        if (typeof intervention.orateur === "string") {
            // TODO try to handle this case
            console.warn(`${logContext}: intervention.orateur is just a string: ${intervention.orateur}`)
            continue;
        }

        if (!intervention.orateur.id_acteur) {
            console.warn(`${logContext}: acteur has no ID: ${JSON.stringify(intervention.orateur)}`);
            continue;
        }

        const { id_acteur } = intervention.orateur;
        
        const existing = detailsByID.get(id_acteur);

        if (!existing) {
            detailsByID.set(id_acteur, intervention.orateur);
            continue;
        }

        if (intervention.orateur.name !== existing.name) {
            console.warn(`${logContext}: diffent names for a same orateur: "${intervention.orateur.name}" and "${existing.name}"`);
        }
    }

    return Array.from(detailsByID.values());
}
