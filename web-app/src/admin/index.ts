/**
 * This is the entrypoint of the admin command line tool
 * which is built using the Rollup config "admin.rollup.config.mjs"
 * and used by the "admin" docker compose service.
 */

import fs from "node:fs";
import { randomBytes } from 'node:crypto';

import yargs from "yargs";
import { hideBin } from "yargs/helpers";

import * as database from "$lib/database";
import * as authentication from "../lib/authentication"
import Database from "better-sqlite3";
import { addSynonym } from "./add-synonyms";
import { mergeTopics } from "./mergeTopics";
import { migration20240630AddCascading } from "./migrations/2024-06-30-add-cascading";
import { migration20240706AddPersons } from "./migrations/2024-07-06-add-persons";
import { importScrappedData } from "./importScrappedData";
import { resetFromBackup } from "./resetFromBackup";
import { migration20241214AddDocDate } from "./migrations/2024-12-14-add-doc-date";

// TODO move each command to its own file

await yargs(hideBin(process.argv))
    .command("import-scrapped-data [--overwrite] [path-to-directory]", "import scrapped data", (yargs)=> {
        yargs
            .positional('path-to-directory', {
                describe: 'path to directory containing the data to import',
                type: 'string',
                default: '/mnt/scrapped-data',
            })
            .boolean('overwrite')
    }, async (argv) => {
        await importScrappedData(argv.pathToDirectory as string, argv.overwrite as boolean);
    })
    // WARNING the first parameter passed to "command" is PARSED by yargs
    // so its format matters; in particular <arg> and [arg] are very different
    .command("reset-from-backup [--delete-existing-scrapped-data] <path-to-backup>", "reset instance from a backup directory", (yargs)=> {
        yargs.boolean('delete-existing-scrapped-data');
        yargs.positional('path-to-backup', {
            describe: 'path to directory containing the backup',
            type: 'string',
            default: '/mnt/backup',
        });
    }, (argv) => {
        resetFromBackup(argv.pathToBackup as string, argv.deleteExistingScrappedData as boolean);
    })
    .command("create-token <username>", "create an authentication token", (yargs) => {
        yargs.positional('username', {
            describe: 'the username that will be used',
            type: 'string',
        })
    }, (argv) => {
        createToken(argv.username as string)
    })
    .command("init-system-db", "", {}, (argv) => {
        if (doesDBExistAlready(authentication.PATH_TO_DB)) {
            console.log(`system DB already exists at ${authentication.PATH_TO_DB}`);
        } else {
            initSystemDB(authentication.PATH_TO_DB);
        }
    })
    .command("migrate-2024-05-05-add-annotation-source-job", "", {}, () => {
        const db = new Database(database.PATH, {fileMustExist: true});
        db.exec("ALTER TABLE docs_labels ADD COLUMN source_job TEXT");
    })
    .command("migrate-2024-06-15-add-synonyms-table", "", {}, () => {
        const db = new Database(database.PATH, {fileMustExist: true});
        db.exec(`CREATE TABLE synonyms (
            name TEXT PRIMARY KEY,
            label_name REFERENCES labels(name) NOT NULL
        )`);
    })
    .command("migrate-2024-06-30-add-cascading", "", {}, migration20240630AddCascading)
    .command("migrate-2024-07-06-add-persons", "", {}, migration20240706AddPersons)
    .command("migrate-2024-12-14-add-doc-date", "", (yargs) => {
        yargs.boolean("allow-rerun");
    }, (argv) => {
        migration20241214AddDocDate(Boolean(argv.allowRerun) as boolean);
    })
    .command("add-synonym <synonym> <label>", "", (yargs) => {
        yargs.positional('synonym', { type: 'string' });
        yargs.positional("label", { type: "string" });
    }, (argv) => {
        addSynonym(argv.synonym as string, argv.label as string);
    })
    .command("merge-topics [--delete-source] <src> <dest>", "", (yargs) => {
        yargs.positional('src', { type: 'string' });
        yargs.positional("dest", { type: "string" });
        yargs.boolean("delete-source");
    }, async (argv) => {
        await mergeTopics(
            argv.src as string,
            argv.dest as string,
            { deleteSource: argv.deleteSource as boolean },
        );
    })
    .command('$0', '', () => {}, (argv) => {
        const cmd = argv._;
        const msg = cmd.length ? `unknown command "${cmd.join(" ")}"` : "command required";
        console.error("ERROR: "+msg);
        process.exit(1);
    })
    .parse();

console.log(
    "If the application hangs you may have to kill it with Ctrl-C."
    +" This is because of the top-level await but I don't know how to fix it."
)


function createToken(username: string) {
    const token = randomBytes(16).toString("base64url");
    const now = new Date();

    const systemDb = new Database(authentication.PATH_TO_DB, {fileMustExist: true});

    systemDb.prepare(`
        INSERT INTO tokens (value, username, created_at)
        VALUES (?, ?, ?)
    `).run(token, username, now.toISOString());

    console.log(`${process.env.DOMAIN}/authn?token=${token}`)
}

function doesDBExistAlready(path: string) {
    try {
        new Database(path, {fileMustExist: true});
        return true
    } catch (error) {
        // @ts-ignore
        if (error.code == "SQLITE_CANTOPEN") {
            return false
        } else {
            throw error;
        }
    }
}

function initSystemDB(path: string) {
    const schema = fs.readFileSync("./databases/system/schema.sql", {encoding: "utf-8"});
    const db = new Database(path);
    db.exec(schema);
}
