import * as readline from 'node:readline/promises';
import { stdin as input, stdout as output } from 'node:process';

import Database from "better-sqlite3";

import * as database from "$lib/database";
import { labelExists } from "./utils";

/**
 * TODO move this to the admin interface of the Web app;
 * also this will be an opportunity to get rid of this "async"
 * that's messing up the admin command;
 */
export async function mergeTopics(
    src: string,
    dest: string,
    options: {
        deleteSource: boolean,
    } = {
        deleteSource: false,
    }
) {
    const db = new Database(database.PATH, {fileMustExist: true});
    
    if (!labelExists(src, db)) {
        throw Error(`source label "${src}" does not exist`)
    }

    if (!labelExists(dest, db)) {
        throw Error(`destination label "${dest}" does not exist`)
    }

    const docsRows = db.prepare(`
        SELECT doc_id, source_job
        FROM docs_labels
        WHERE label_name = ?
    `).all(src) as {
        doc_id: string, source_job: string,
    }[];

    const topicsRows = db.prepare(`
        SELECT child_label_name
        FROM labels_parent_labels
        WHERE parent_label_name = ?
    `).all(src) as {
        child_label_name: string,
    }[];

    if (topicsRows.some(row => (row.child_label_name == dest))) {
        console.log(`WARNING: destination "${dest}" is a child of source "${src}"`);
        throw Error("TODO implement");
    }

    if (docsRows?.length) {
        console.log(`The following documents will be moved from "${src}" to "${dest}":`);
        for (const row of docsRows) {
            console.log(
                "- " + row.doc_id
                + (row.source_job ? ` (from job ${row.source_job})` : "")
            );
        }
    } else {
        console.log(`No documents to move from "${src}" to "${dest}".`);
    }

    console.log();

    if (topicsRows?.length) {
        console.log(`The following topics will be moved from "${src}" to "${dest}":`);
        for (const row of topicsRows) {
            console.log("- " + row.child_label_name);
        }
    } else {
        console.log(`No topics to move from "${src}" to "${dest}".`);
    }

    console.log();

    if (options.deleteSource) {
        console.log(`"${src}" WILL BE DELETED`)
    } else {
        console.log(`source topic will not be deleted`)
    }

    console.log();
    
    const rl = readline.createInterface({ input, output });
    await rl.question("Hit <Enter> to confirm, <Ctrl-C> to abort");
    console.log();

    for (const { doc_id } of docsRows) {
        console.log(`moving ${ doc_id }`);

        try {
            const result = db.prepare(`
                UPDATE docs_labels
                SET label_name = ?
                WHERE label_name = ? AND doc_id = ?
            `).run(dest, src, doc_id);

            if (result.changes != 1) {
                throw Error(`expected 1 row changed, got ${result.changes}`);
            }
        } catch (error) {
            // @ts-ignore
            if (error.code == "SQLITE_CONSTRAINT_PRIMARYKEY") {
                console.log(`moving "${doc_id}": was already annotated with "${dest}"; deleting annotation instead`)

                const result = db.prepare(`
                    DELETE from docs_labels
                    WHERE label_name = ? AND doc_id = ?
                `).run(src, doc_id);
    
                if (result.changes != 1) {
                    throw Error(`deleting annotation for "${doc_id}" expected 1 row changed, got ${result.changes}`);
                }

                continue;
            }

            if (error instanceof Error) {
                error.message = `moving doc "${doc_id}": `+error.message;
            }

            throw error;
        }
    }

    for (const { child_label_name } of topicsRows) {
        console.log(`moving ${ child_label_name }`);

        try {
            const result = db.prepare(`
                UPDATE labels_parent_labels
                SET parent_label_name = ?
                WHERE parent_label_name = ? AND child_label_name = ?
            `).run(dest, src, child_label_name);

            if (result.changes != 1) {
                throw Error(`expected 1 row changed, got ${result.changes}`)
            }
        } catch (error) {
            // @ts-ignore
            if (error.code == "SQLITE_CONSTRAINT_PRIMARYKEY") {
                console.log(`moving "${child_label_name}": was already annotated sub-topic of "${dest}"; deleting instead`)

                const result = db.prepare(`
                    DELETE FROM labels_parent_labels
                    WHERE parent_label_name = ? AND child_label_name = ?
                `).run(src, child_label_name);
    
                if (result.changes != 1) {
                    throw Error(`deleting parent relation for "${child_label_name}" expected 1 row changed, got ${result.changes}`);
                }

                continue;
            }

            if (error instanceof Error) {
                error.message = `moving topic "${child_label_name}": `+error.message;
            }

            throw error;
        }
    }

    if (options.deleteSource) {
        try {
           db.prepare(`
                DELETE FROM labels_parent_labels
                WHERE child_label_name = ?
            `).run(src);
        } catch (error) {
            if (error instanceof Error) {
                error.message = `deleting relations of "${src}" as child topic: `+error.message;
            }
            
            throw error;
        }

        try {
            db.prepare(`
                DELETE FROM synonyms
                WHERE label_name = ?
            `).run(src);
        } catch (error) {
            if (error instanceof Error) {
                error.message = `deleting synonyms pointing to "${src}": `+error.message;
            }
            
            throw error;
        }

        try {
            db.prepare(`
                DELETE FROM forced_term_weights
                WHERE label_name = ?
            `).run(src);
        } catch (error) {
            if (error instanceof Error) {
                error.message = `deleting forced weights pointing to "${src}": `+error.message;
            }
            
            throw error;
        }

        try {
            const result = db.prepare(`
                DELETE FROM labels
                WHERE name = ?
            `).run(src);

            if (result.changes != 1) {
                throw Error(`expected 1 row changed, got ${result.changes}`)
            }
        } catch (error) {
            if (error instanceof Error) {
                error.message = `deleting topic "${src}": `+error.message;
            }
            
            throw error;
        }
    }
}
