import fs from "node:fs";

import Database from "better-sqlite3";

import { printDBSchema } from "../utils";
import * as database from "$lib/database";

const TABLES_BY_MODE = {
    "strict": ["labels", "docs", "docs_labels","labels_parent_labels"],
    "lax": ["synonyms", "forced_term_weights"],
}

export function migration20240630AddCascading() {
    const db = new Database(database.PATH, {fileMustExist: true});

    try {
        db.exec("DROP TABLE poldocs")
    } catch (error) {
        console.warn(`(non-fatal) dropping table "poldocs": ${error}`)
    }

    // note that the order of tables matters
    // for foreign keys to work

    for (const [mode, tables] of Object.entries(TABLES_BY_MODE)) {
        for (const table of tables) {
            try {
                db.exec(`ALTER TABLE ${table} RENAME TO old_${table}`);
            } catch (error) {
                if (mode == "lax") {
                    console.error(`(non-fatal) renaming table ${table}: ${error}`)
                    continue;
                } else {
                    if (error instanceof Error) {
                        error.message = `renaming old table ${table}: ${error.message}`;
                    }
                    throw error;
                }
            }
        }
    }

    const schema = fs.readFileSync("./databases/migrations/main/2024-06-30-add-cascading.sql", {encoding: "utf-8"});

    for (const part of schema.split("\n\n")) {
        try {
            db.exec(part);
        } catch (error) {
            if (error instanceof Error) {
                error.message = [
                    "executing new schema:",
                    `schema part: ${part}`,
                    `error: ${error.message}`,
                ].join("\n");
            }

            throw error;
        }
    }

    for (const [mode, tables] of Object.entries(TABLES_BY_MODE)) {
        for (const table of tables) {
            // we are adding a column in table "docs"
            // so we have to make columns explicit when copying
            const columns = table == "docs" ? "(id, title)" : "";

            try {
                db.exec(`INSERT INTO ${table} ${columns} SELECT * FROM old_${table}`);
            } catch (error) {
                if (error instanceof Error) {
                    error.message = `copying old_${table} into ${table}: ${error.message}`
                }

                if (mode == "lax") {
                    console.error(`(non-fatal): ${error}`)
                    continue;
                }

                throw error
            }
        }
    }

    // note that we delete tables in the reverse order in which they get created,
    // again to avoid foreign key errors

    for (const [mode, tables] of Object.entries(TABLES_BY_MODE).reverse()) {
        for (const table of tables.reverse()) {
            try {
                db.exec(`DROP TABLE old_${table}`);
                console.log(`dropped table old_${table}`);
            } catch (error) {
                if (error instanceof Error) {
                    error.message = `dropping old_${table}: ${error.message}`
                }

                if (mode == "lax") {
                    console.error(`(non-fatal): ${error}`)
                    continue;
                }

                throw error
            }
        }
    }

    console.log("done. New schema:")
    printDBSchema(db);
}