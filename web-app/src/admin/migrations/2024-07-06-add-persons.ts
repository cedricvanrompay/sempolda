import Database from "better-sqlite3";

import * as database from "$lib/database";

export function migration20240706AddPersons() {
    const db = new Database(database.PATH, {fileMustExist: true});
    db.exec(`
        CREATE TABLE persons (
            id TEXT PRIMARY KEY,
            name TEXT NOT NULL
        );

        CREATE TABLE assemble_nationale_acteurs_persons (
            id_acteur TEXT NOT NULL,
            person_id NOT NULL REFERENCES persons(id),
            UNIQUE (id_acteur),
            UNIQUE (person_id)
        );

        CREATE TABLE docs_persons (
            doc_id NOT NULL REFERENCES docs(id) ON DELETE CASCADE,
            person_id NOT NULL REFERENCES persons(id) ON UPDATE CASCADE ON DELETE CASCADE
        );
    `);
}