import Database from "better-sqlite3";

import * as database from "$lib/database";

const dateRegexp = new RegExp("[0-9]{4}-[0-9]{2}-[0-9]{2}");

export function migration20241214AddDocDate(allowRerun: boolean = false) {
    const db = new Database(database.PATH, {fileMustExist: true});

    try {
        db.exec(`
            ALTER TABLE docs
            ADD COLUMN date TEXT
        `)
    } catch (error) {
        if (!allowRerun) {
            throw error;
        }
    }

    const docs = db.prepare(`
        SELECT * FROM docs
    `).all() as Doc[];

    let nbSuccesses = 0;
    let nbErrors = 0;
    for (const doc of docs) {
        try {
            processOneDoc(doc, db);
            nbSuccesses++

            if (nbSuccesses % 50 == 0) {
                console.log(`${nbSuccesses} rows migrated successfully so far...`)
            }
        } catch (error) {
            console.error(`ERROR: ${doc.id}: ${error}`);

            nbErrors++
            if (nbErrors > 20) {
                throw new Error("too many errors");
            }
        }
    }
}

type Doc = {
    id: string
    title: string
    // in practice at this time we don't use this column,
    // but just in case
    official_source_url: string
}

function processOneDoc(doc: Doc, db: Database.Database) {
    const parts = doc.id.split("/");

    if (
        parts.length != 4
        || parts[0] != "assemblee-nationale" && parts[1] != "questions-gouv"
    ) {
        console.log(`skipping ${doc.id}`);
        return;
    }

    const date = parts[2];

    console.assert(date.match(dateRegexp));

    const result = db.prepare(`
        UPDATE docs
        SET date = ?
        WHERE id = ?
    `).run(date, doc.id);

    if (result.changes != 1) {
        throw Error(`expected 1 row changed, got ${result.changes}`);
    }
}