import path from "node:path";
import fs from "node:fs";
import * as database from "$lib/database";

export function resetFromBackup(pathToBackup: string, deleteExistingScrappedData: boolean) {
    // no need to use SQLite's "backup API"
    // since the source database is not supposed to be in use
    fs.copyFileSync(path.join(pathToBackup, "database.db"), database.PATH);

    if (deleteExistingScrappedData) {
        fs.rmSync(database.SCRAPPED_DATA_PATH, {recursive: true});
    }

    fs.cpSync(
        path.join(pathToBackup, "scrapped"),
        database.SCRAPPED_DATA_PATH,
        {
            recursive: true,
        }
    );

    // TODO copy auto-annotation job logs
}