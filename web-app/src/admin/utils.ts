import type Database from "better-sqlite3";

export function labelExists(label: string, db: Database.Database) {
    const rows = db.prepare(`
        SELECT name FROM labels
        WHERE name=?
    `).get(label);

    if (rows) {
        return true;
    } else {
        return false;
    }
}

export function printDBSchema(db: Database.Database) {
    for (const table of db.prepare(`
        SELECT sql FROM sqlite_master WHERE type='table'
    `).all() as {sql: string}[]
    ) {
        console.log(table.sql);
    };
}

export function prependContextToError(context: string, error: any) {
    if (error instanceof Error) {
        error.message = `${context}: ${error.message}`
    }

    return error;
}