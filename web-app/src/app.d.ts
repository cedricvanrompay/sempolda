// See https://kit.svelte.dev/docs/types#app

import type { Autocompleter } from "$lib/autocompletion";
import type { MainDb } from "$lib/database";

// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			mainDb: MainDb,
			account: AuthenticatedAccount,
			autocompleters: {
				persons: Autocompleter<Person>,
				// TODO make the autocompleter return whether the topic is a synonym or not
				topics: Autocompleter<string>,
			},
		}
		// interface PageData {}
		// interface PageState {}
		// interface Platform {}
	}
}

export {};
