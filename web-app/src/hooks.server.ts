import { SystemDb, Unauthenticated } from "$lib/authentication";
import { Autocompleter } from "$lib/autocompletion";
import { MainDb } from "$lib/database";
import statusCodes from "$lib/statusCodes";
import type { Person } from "$lib/types";
import type { Handle } from "@sveltejs/kit";

const publicPaths = new Set([
    '/',
    '/authn',
    "/api/topic-tree",
    "/api/autocomplete/topics",
    "/api/autocomplete/persons",
    "/api/questions-gouv-sessions",
]);

const publicPathPrefixes = new Array<string>(
    "/assemblee-nationale/",
    "/docs",
);

let topicAutocompleter: Autocompleter<string>;
let personAutocompleter: Autocompleter<Person>;

export const handle: Handle = async ({ event, resolve }) => {
    const systemDb = new SystemDb()

    try {
        event.locals.account = systemDb.getAuthenticatedAccount(event.cookies);
    } catch (error) {
        if (!(error instanceof Unauthenticated)) {
            throw error;
        }
    }

    if (
        !(isPublic(event.url.pathname) && event.request.method == 'GET')
        && !event.locals.account
    ) {
        return new Response(
            "<p>Unauthorized</p>",
            {
                status: statusCodes.Unauthorized,
                headers: {
                    "content-type": "text/html",
                }
            }
        );
    }

    event.locals.mainDb = new MainDb();

    event.locals.autocompleters = {
        topics: getTopicAutocompleterSingleton(event.locals.mainDb),
        persons: getPersonAutocompleterSingleton(event.locals.mainDb),
    };

    const response = await resolve(event);
    return response;
}

function isPublic(path: string) {
    if (publicPaths.has(path)) {
        return true;
    }

    for (const prefix of publicPathPrefixes) {
        if (path.startsWith(prefix)) {
            return true;
        }
    }

    return false;
}

function getTopicAutocompleterSingleton(db: MainDb) {
    // note that "topicAutocompleter" is defined in the module scope
    // (this is a "singleton")

    if (topicAutocompleter == undefined) {
        topicAutocompleter = new Autocompleter();

        let rows = db.connection.prepare(`
            SELECT name FROM labels
        `).all() as { name: string}[];

        for (const row of rows) {
            topicAutocompleter.add(row.name, row.name);
        }

        rows = db.connection.prepare(`
            SELECT name FROM synonyms
        `).all() as { name: string}[];

        for (const row of rows) {
            topicAutocompleter.add(row.name, row.name);
        }
    }

    return topicAutocompleter;
}


function getPersonAutocompleterSingleton(db: MainDb) {
    // note that "personAutocompleter" is defined in the module scope
    // (this is a "singleton")

    if (personAutocompleter == undefined) {
        personAutocompleter = new Autocompleter<Person>();

        for (const person of db.getAllPersons()) {
            personAutocompleter.add(person.name, person);
        }
    }

    return personAutocompleter;
}