import type { Topic } from "$lib/types";

export class TopicDoesNotExist extends Error {
    topicName: string

    constructor(topicName: string) {
        super(topicName);
        this.name = this.constructor.name;
        this.topicName = topicName;
    }
}

export type TopicCreation = {
    name: string
    resolve: (value: Topic | PromiseLike<Topic>) => void
    reject: (reason: any) => void
}