export type ConfirmationPromise = {
    resolve: (value?: unknown) => void
    reject: (reason: any) => void
}