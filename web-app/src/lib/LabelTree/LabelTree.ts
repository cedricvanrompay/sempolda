export type LabelTreeNode = {
    name: string
    children: string[]
};

export async function getLabelTree(root?: string | null) {
    const url = '/api/topic-tree' + (
        root ? `?root=${root}` : ''
    )

    const response = await fetch(url);

    return response.json()
}