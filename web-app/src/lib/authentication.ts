import type { Cookies } from "@sveltejs/kit";
import Database from "better-sqlite3";
import { randomBytes } from 'node:crypto';

export const PATH_TO_DB = "/var/lib/sempolda/system.db";

export const COOKIE_MAX_AGE_SECONDS = 60 * 24 * 3600; // 60 days

export class AuthenticatedAccount {
    username: string

    constructor(username: string) {
        this.username = username;
    }
}

export class Unauthenticated extends Error {
    constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
    }
};

export class ExpiredCredentials extends Unauthenticated {
    constructor() {
        super("expired credentials");
        this.name = this.constructor.name;
    }
}

export class SystemDb {
    connection: Database.Database

    constructor(path: string = PATH_TO_DB) {
        this.connection = new Database(path);
    }

    /**
     * @param cookies will be mutated
     */
    createCookieFromToken(token: string, cookies: Cookies) {
        const row = this.connection.prepare(`
            SELECT username, created_at FROM tokens
            WHERE value=?
        `).get(token) as null | {
            username: string,
            created_at: string,
        };

        if (!row) {
            throw new Unauthenticated("token not found in database");
        }

        const username = row.username;

        this.connection.prepare(`
            DELETE FROM tokens
            WHERE value=?
        `).run(token);

        const createdAt = Date.parse(row.created_at);
        const tokenAgeSeconds = (Date.now() - createdAt) / 1000;

        if (tokenAgeSeconds > COOKIE_MAX_AGE_SECONDS) {
            throw new ExpiredCredentials();
        }

        const cookie = randomBytes(16).toString("base64");
        const now = new Date();

        this.connection.prepare(`
            INSERT INTO cookies (value, username, created_at)
            VALUES (?, ?, ?)
        `).run(cookie, username, now.toISOString());

        cookies.set("session", cookie, {
            httpOnly: true,
            sameSite: 'strict',
            path: '/',
            secure: true,
            maxAge: COOKIE_MAX_AGE_SECONDS,
        })

        return username;
    }

    getAuthenticatedAccount(cookies: Cookies) {
        const cookie = cookies.get("session");

        if (!cookie) {
            throw new Unauthenticated("no session cookie");
        }

        const row = this.connection
            .prepare(`
                SELECT username, created_at
                FROM cookies
                WHERE value=?
            `).get(cookie) as {
                username: string,
                created_at: string,
            } | undefined;

        if (!row) {
            throw new Unauthenticated("cookie not found in database");
        }

        const createdAt = Date.parse(row.created_at);
        const cookieAgeSeconds = (Date.now() - createdAt) / 1000;
        if (cookieAgeSeconds > COOKIE_MAX_AGE_SECONDS) {
            throw new Unauthenticated("cookie expired");
        }

        return new AuthenticatedAccount(row.username);
    }

}
