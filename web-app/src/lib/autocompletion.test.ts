import { expect, test } from 'vitest';

import { Autocompleter } from './autocompletion';


test('gives expected result in toy case', () => {
    const autocompleter = new Autocompleter<string>();

    for (const x of ["foo", "bar", "baz"]) {
        autocompleter.add(x, x);
    }

    expect(autocompleter.complete("bar")).toStrictEqual([
        {
            key: "bar",
            score: 100,
            value: "bar",
        },
        {
            key: "baz",
            score: 30,
            value: "baz",
        },
        // note that because "foo" has none of the expected n-grams,
        // it does not even appear in the suggestions
    ])
});


test('gives expected result with realistic case', () => {
    const autocompleter = new Autocompleter<string>();

    for (const x of [
        "famille", "famine", "GAFAMs", "amérique",
        "mines d'or", "mineurs délinquants",
        "foncier", "éducation",
        "insertion par l’activité économique",
    ]) {
        autocompleter.add(x, x);
    }

    expect(autocompleter.complete("fam")).toStrictEqual([
        {
            key: "famine",
            score: 70.71067811865476,
            value: "famine",
        },
        {
            key: "GAFAMs",
            score: 70.71067811865476,
            value: "GAFAMs",
        },
        {
            key: "famille",
            score: 65.46536707079771,
            value: "famille",
        },
        {
            key: "amérique",
            score: 18.371173070873834,
            value: "amérique",
        },
        {
            key: "mineurs délinquants",
            score: 3.9735970711951305,
            value: "mineurs délinquants",
        }
    ])
});


test('gives a score of 100 for an exact match', () => {
    for (const x of [
        "lol",
        "longer",
        "Tricky! ,é→ø’«ł",
        "especially loooong",
        "reapeatrepeat",
        "aaaaaaaa",
    ]) {
        const autocompleter = new Autocompleter<string>();
        autocompleter.add(x, x);
        expect(autocompleter.complete(x)[0]).toStrictEqual({
            key: x,
            score: 100,
            value: x,
        });
    }
})


test('does not give a higher score for a longer candidate', () => {
    for (const [x, yy] of [
        ["x", ["xxx", "xx xx"]],
        ["lol", ["lollollol", "lololol"]],
        ["repeat", ["repeatrepeat"]]
    ] as const) {
        const autocompleter = new Autocompleter<string>();

        autocompleter.add(x, x);

        for (const y of yy) {
            autocompleter.add(y, y);
        }

        for (const { value: candidate, score } of autocompleter.complete(x)) {
            if (candidate === x) {
                continue;
            }

            expect(score, `score for ${candidate}`).toBeLessThan(100);
        }
    }
})