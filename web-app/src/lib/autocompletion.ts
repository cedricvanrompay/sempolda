export type Completion<T> = {
    key: string
    score: number
    value: T
} 

export class Autocompleter<T> {
    nGramMap: Map<string, Array<{ key: string, nGramCount: number }>>
    map: Map<string, T>

    constructor() {
        this.nGramMap = new Map();
        this.map = new Map();
    }

    add(key: string, value: T) {
        if (this.map.has(key)) {
            throw Error(`key "${key}" is already present`);
        }

        const ngrams = computeNGrams(key);
        for (const [ngram, count] of ngrams.entries()) {
            let countPerEntry = this.nGramMap.get(ngram);
            if (!countPerEntry) {
                countPerEntry = new Array<{ key: string, nGramCount: number }>();
                this.nGramMap.set(ngram, countPerEntry);
            }
            
            countPerEntry.push({
                key,
                nGramCount: count,
            })
        }

        this.map.set(key, value);
    }

    complete(key: string) {
        const nGrams = computeNGrams(key);

        let maxScore = 0;
        for (const [ngram, count] of nGrams.entries()) {
            maxScore += count * (ngram.length ** 2) / Math.sqrt(key.length);
        }

        const candidates = new Map<string, number>();

        for (const [ngram, count] of nGrams.entries()) {
            const matches = this.nGramMap.get(ngram);

            if (!matches) {
                continue;
            }

            for (const match of matches) {
                // if the input had N times this ngram,
                // the candidate cannot get extra points
                // for having more than N occurences of this ngram
                const matchCount = Math.min(count, match.nGramCount);

                const points = (
                    matchCount 
                    * (ngram.length ** 2)
                    // we divide by the *square root* of the length
                    // so that small values are not too advantaged
                    / Math.sqrt(match.key.length)
                )

                // TODO do not insert candidate
                // if it has no chances of being among the ones returned

                candidates.set(
                    match.key,
                    (candidates.get(match.key) || 0) + points
                )
            }
        }

        const sorted = (
            Array.from(candidates.entries())
                // sorting by *descending* score
                .sort((a, b) => b[1] - a[1])
        );

        const result = Array<Completion<T>>();

        for (const [key, score] of sorted.slice(0, 5)) {
            const value = this.map.get(key);

            if (!value) {
                throw Error(`found key ${key} in ngram map but no corresponding value in map`);
            }

            result.push({
                key,
                score: 100 * score / maxScore,
                value,
            })
        }

        return result;
    }
}

const MAX_NGRAM_LENGTH = 4;

function computeNGrams(topic: string) {
    const folded = topic.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    
    const nGramCounter = new Map<string, number>();
    for (const token of folded.split(' ')) {
        for (let n = 1; n <= MAX_NGRAM_LENGTH; n++) {
            if (token.length < n) {
                continue;
            }

            for (let i = 0; i <= token.length - n; i++) {
                const ngram = token.slice(i, i+n)
                const currentCount = nGramCounter.get(ngram) || 0;
                nGramCounter.set(ngram, currentCount+1)
            }
        }
    }

    return nGramCounter;
}