import path from "node:path";
import { readFile } from 'node:fs/promises';

export type Content = {
    paragraphs: string[];
}

export async function getPoldocContent(id: string) {
    const p = path.join( `/var/lib/sempolda/scrapped`, id, "content.json");

    let fileText: string;
    try {
        fileText = await readFile(p, { encoding: "utf-8" });
    } catch (error: any) {
        if (error.code === "ENOENT") {
            throw new Error(`file not found: ${p}`)
        }

        throw error;
    }

    const parsed = JSON.parse(fileText);

    if (!(parsed instanceof Object)) {
        throw new Error(`not a JSON object: ${p}`);
    }

    if (!parsed.paragraphs) {
        throw new Error(`no "paragraphs" field in ${p}`)
    }

    return parsed as Content;
}