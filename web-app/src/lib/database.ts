import Database from "better-sqlite3";
import type { Person, Poldoc, Topic } from "./types";
import { sortPoldocList } from "./utils";

export const PATH = "/var/lib/sempolda/database.db";
export const SCRAPPED_DATA_PATH = "/var/lib/sempolda/scrapped";

// TODO try to split in several files

export class MainDb {
    connection: Database.Database

    constructor(
        path: string = PATH,
        {
            fileMustExist
        }: {
            fileMustExist: boolean,
        } = {
                fileMustExist: false,
            },
    ) {
        this.connection = new Database(path, { fileMustExist });
    }

    getRootLabelsNames() {
        const rows = this.connection.prepare(`
            SELECT name
            FROM labels x LEFT JOIN labels_parent_labels y
            ON x.name = y.child_label_name
            WHERE y.child_label_name IS NULL;
        `).all() as Array<{ name: string }>;

        return rows.map(x => x.name);
    }

    getChildrenNames(parent: string) {
        return this.connection
            .prepare(`
                SELECT name
                FROM labels x LEFT JOIN labels_parent_labels y
                ON x.name = y.child_label_name
                WHERE y.parent_label_name = ?;
            `)
            .all(parent)
            .map((row: any) => row.name as string)
    }

    getSynonym(name: string) {
        const row = this.connection
            .prepare(`
                SELECT name, label_name FROM synonyms
                WHERE name = ?
            `).get(name) as {
                name: string,
                label_name: string,
            } || undefined;

        if (!row) {
            return null;
        }

        return row.label_name;
    }

    getTopic(name: string) {
        const row = this.connection
            .prepare(`
                SELECT * FROM labels
                WHERE name=?
            `)
            .get(name) as {
                name: string,
                description: string,
            } || undefined;

        if (!row) {
            throw new NoResult(name);
        }

        const result: Topic = {
            name: row.name,
            description: row.description,
            parents: [],
        }

        const rows = this.connection
            .prepare(`
                SELECT parent_label_name FROM labels_parent_labels
                WHERE child_label_name=?
            `)
            .all(name) as {
                parent_label_name: string
            }[];

        for (const row of rows) {
            result.parents.push(row.parent_label_name)
        }

        for (const row of this.connection.prepare(`
                SELECT name FROM synonyms WHERE label_name = ?
            `).all(name) as { name: string }[]) {
            if (!result.synonyms) {
                result.synonyms = [];
            }

            result.synonyms.push(row.name);
        };

        return result;
    }

    getSubtopicsNames(root: string) {
        const rows = this.connection
            .prepare(`
                SELECT child_label_name FROM labels_parent_labels
                WHERE parent_label_name = ?
            `).all(root) as { child_label_name: string }[];

        return rows.map(row => row.child_label_name);
    }

    getPoldocsIdsByLabel(label: string) {
        const rows = this.connection
            .prepare(`
                SELECT doc_id from docs_labels
                WHERE label_name = ?
            `).all(label) as { doc_id: string }[];

        return rows.map(row => row.doc_id);
    }

    getPoldoc(id: string) {
        const row = this.connection
            .prepare(`
                SELECT id, title, date, official_source_url FROM docs 
                WHERE id=?
            `).get(id) as {
                id: string,
                title: string
                date: string
                official_source_url: string,
            } || undefined;

        if (!row) {
            throw new NoResult(id);
        }

        const annotations = this.connection
            .prepare(`
                SELECT label_name, source_job FROM docs_labels
                WHERE doc_id=?
            `).all(id)
            .map((row: any) => ({
                topic: row.label_name as string,
                source_job: row.source_job as string | null,
            }));

        const persons = this.connection
            .prepare(`
                SELECT persons.id, persons.name
                FROM docs_persons JOIN persons
                WHERE docs_persons.doc_id = ?
                AND docs_persons.person_id = persons.id
            `).all(id) as {
                id: string
                name: string
            }[];

        return {
            id: row.id,
            title: row.title,
            date: row.date,
            official_source_url: row.official_source_url,
            annotations,
            persons,
        }
    }

    /**
     * result is sorted by date, most recent first
     * 
     * Note that this function does not check if the document has topics linked to it
    */
    listLatestPoldocs() {
        const rows = this.connection
            .prepare(`
            SELECT id, title FROM docs
            ORDER BY date DESC
            LIMIT 35 
        `).all() as { id: string }[];

        if (rows.length === 0) {
            throw new Error("no rows");
        }

        const result = new Array<Poldoc>();

        for (const row of rows) {
            const doc = this.getPoldoc(row.id);
            result.push(doc);
        }

        return result;
    }

    getPrevAndNextPoldoc(id: string) {
        const parts = id.split("/");
        const questionNb = Number(parts.slice(-1));

        const statement = this.connection.prepare(`
            SELECT title
            FROM docs
            WHERE id=?
        `)

        function poldocOrNull(id: string) {
            const row = statement.get(id) as {
                title: string,
            } | undefined;

            if (row) {
                return {
                    id: id,
                    title: row.title,
                }
            } else {
                return null;
            }
        }

        const idPrefix = parts.slice(0, -1).join("/") + "/"

        return {
            previous: poldocOrNull(idPrefix + String(questionNb - 1).padStart(2, '0')),
            next: poldocOrNull(idPrefix + String(questionNb + 1).padStart(2, '0'))
        }
    }

    insertPerson(p: Person) {
        const result = this.connection.prepare(`
            INSERT INTO persons(id, name)
            VALUES (?, ?)
        `).run(p.id, p.name);

        if (result.changes != 1) {
            throw Error(`expected 1 row changed, got ${result.changes}`)
        }
    }

    getAllPersons() {
        return this.connection.prepare(`
            SELECT * FROM persons
        `).all() as Person[];
    }

    getPerson(id: string) {
        return this.connection.prepare(`
            SELECT * FROM persons
            WHERE id = ?
        `).get(id) as Person | undefined;
    }

    getPersonbyName(name: string) {
        return this.connection.prepare(`
            SELECT * FROM persons
            WHERE name = ?
        `).get(name) as Person | undefined;
    }

    insertDocPerson(docId: string, personId: string) {
        const result = this.connection.prepare(`
            INSERT INTO docs_persons(doc_id, person_id)
            VALUES (?, ?)
        `).run(docId, personId);

        if (result.changes != 1) {
            throw Error(`expected 1 row changed, got ${result.changes}`)
        }
    }

    /**
     * result is sorted by date, most recent first
     */
    getPoldocsByPerson(personId: string) {
        // TODO do this in a single SQL query?

        const rows = this.connection.prepare(`
            SELECT docs.id
            FROM docs INNER JOIN docs_persons
            ON docs.id = docs_persons.doc_id
            WHERE docs_persons.person_id = ?
        `).all(personId) as { id: string }[];

        const poldocs = rows.map(({ id }) => this.getPoldoc(id));
        sortPoldocList(poldocs);
        return poldocs;
    }

    insertAssembleeNationaleActor(id_acteur: string, person: Person) {
        const result = this.connection.prepare(`
            INSERT INTO assemble_nationale_acteurs_persons(id_acteur, person_id)
            VALUES (?, ?)
        `).run(id_acteur, person.id);

        if (result.changes != 1) {
            throw Error(`expected 1 row changed, got ${result.changes}`)
        }
    }

    getAssembleeNationaleActor(id_acteur: string) {
        return this.connection.prepare(`
            SELECT id, name
            FROM persons INNER JOIN assemble_nationale_acteurs_persons as acteurs
            ON acteurs.person_id = persons.id
            WHERE acteurs.id_acteur = ?
        `).get(id_acteur) as Person | undefined;
    }
}

export class NoResult extends Error {
    id: any

    constructor(id: any) {
        super(`no result with ID "${id}"`)
        this.name = this.constructor.name;
        this.id = id;
    }
};
