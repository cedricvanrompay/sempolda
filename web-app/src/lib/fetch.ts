export class HTTPError extends Error {
  response: Response

  constructor(response: Response) {
    super(`HTTP ${response.status} ${response.statusText}`);
    this.name = this.constructor.name;
    this.response = response;
  }
}

export const customFetch = (rawFetch: typeof fetch) => async (
    url: string,
    init?: any,
    { assertOK=true } = {}
) => {
  init = init || {};

  if (init?.body) {
    // JSON formatting
    init.body = JSON.stringify(init.body);
    init.headers = new Headers(init.headers);
    init.headers.append('Content-Type', 'application/json');
  }

  const response = await rawFetch(url, init);

  if (assertOK && !response.ok) {
    throw new HTTPError(response);
  }

  return response;
}