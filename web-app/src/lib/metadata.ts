import fs from "node:fs";
import path from "node:path";
import { SCRAPPED_DATA_PATH } from "./database";

export function getMetadata(poldocId: string) {
    const p = path.join(SCRAPPED_DATA_PATH, poldocId, "metadata.json")

    const text = fs.readFileSync(p, { encoding: "utf-8" });

    return JSON.parse(text) as Metadata;
}

/**
 * the data structure of the "metadata.json" file
 * that's stored next to the scrapped document.
 */
export type Metadata = {
    title: string
    date: string
    scrapped_from: {
        url: string
        sha256: string
    }
    scrapped_time?: string
    official_source_url?: string
}