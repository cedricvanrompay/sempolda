import { readFile } from 'node:fs/promises';
import type { Intervention } from './types';

export async function getInterventions(id: string): Promise<Intervention[]> {
    const dir = `/var/lib/sempolda/scrapped/${id}`;

    const interventionsPath = dir+`/interventions.json`

    try {
        return JSON.parse(await readFile(interventionsPath, { encoding: "utf-8" }));
    } catch (error: any) {
        if (error.code === 'ENOENT') {
            // TODO migrate all data to new format
            const legacyContentPath = dir+'/content.json';
            const json = JSON.parse(await readFile(legacyContentPath, { encoding: "utf-8" }));
            return json.map(({orateur, texte}: any) => ({
                orateur,
                paragraphs: [texte],
            }))
        } else {
            throw error;
        }
    }
}