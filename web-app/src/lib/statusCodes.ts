export default {
    /**
     * in practice, this code is used
     * to mean "unauthenticated"
     */
    Unauthorized: 401,
}