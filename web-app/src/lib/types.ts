export type Label = {
    name: string
    description: string
}

export type Poldoc = {
    id: string
    title: string
    date: string
    annotations?: Annotation[]
    persons?: Person[]
    official_source_url?: string
}

export type Annotation = {
    topic: string
    source_job: string | null
}

export type Topic = {
    name: string
    description: string
    parents: string[]
    synonyms?: string[]
}

export class CanceledByUser extends Error {
    constructor(message?: string) {
        super(message);
        this.name = this.constructor.name;
    }
}

export type OrateurDetails =  {
    name: string
    id_acteur: string
    status?: string | null
}

export type Orateur = string | OrateurDetails

export type Intervention = {
    paragraphs: string[]
    type?: "question_or_answer" | "interruption" | "president",
    orateur?: Orateur
}

/**
 * stored in the "persons" table
 */
export type Person = {
    id: string
    name: string
}

/**
 * suggestion from the auto-annotation service
 */
export type Suggestion = {
    topic_name: string
    score: number
    /**
     * map from term to weight and number of occurences
     */
    tokens_contributions: {
        /**
         * values are weight and number of occurences
         */ 
        [term: string]: [number, number]
    }
}

export const POLDOC_TYPE_PREFIXES = {
    questionGouv: "assemblee-nationale/questions-gouv",
    politico: "politico",
}
