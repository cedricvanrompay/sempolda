import type { Poldoc } from "./types";

export function approx(x: number, decimalPlaces: number) {
    let n = decimalPlaces;
    return Math.floor(x * (10 ** n)) / (10 ** n)
}

/** sorts in place by date, most recent first
 * 
 * XXX assumes all poldocs have their ID starting with "assemblee-nationale/questions-gouv/[date]"
*/
export function sortPoldocList(poldocs: Poldoc[]) {
    poldocs.sort((a, b) => (a.id === b.id ? 0 : a.id < b.id ? 1 : -1))
}