import { getPoldocContent } from '$lib/content';
import type { PageServerLoad } from './$types';

export const load = (async ({ params, locals: { mainDb } }) => {
    const poldoc = mainDb.getPoldoc(params.id);

    return {
        poldoc,
        content: await getPoldocContent(params.id),
    };
}) satisfies PageServerLoad;

