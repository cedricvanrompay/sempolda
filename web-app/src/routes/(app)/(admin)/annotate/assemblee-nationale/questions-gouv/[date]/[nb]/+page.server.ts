import { fail } from '@sveltejs/kit';

import { getInterventions } from '$lib/questionsGouv';
import { POLDOC_TYPE_PREFIXES, type Suggestion } from '$lib/types';
import type { Actions, PageServerLoad } from './$types';
import { NoResult } from '$lib/database';

export const load: PageServerLoad = async ({ params, locals: { mainDb } }) => {
    const { date, nb } = params;

    const id = `${POLDOC_TYPE_PREFIXES.questionGouv}/${date}/${nb}`;

    const poldoc = mainDb.getPoldoc(id);
    const prevAndNext = mainDb.getPrevAndNextPoldoc(id);

    return {
        poldoc,
        ...prevAndNext,
        content: await getInterventions(id),
    }
}

export const actions = {
    "get-suggestions": async ({request, params: { date, nb }}) => {
        const data = await request.formData();

        const tfidfOnly = Boolean(data.get("tfidf-only"));

        const id = `${POLDOC_TYPE_PREFIXES.questionGouv}/${date}/${nb}`;

        return {
            suggestions: await getSuggestions(id, tfidfOnly)
        };
    },

	"poldoc-topic-score": async ({request, params: { date, nb }}) => {
        const data = await request.formData();

        const topic: string = data.get('topic') as string;
        if (!topic) {
            return {};
        }

        const id = `${POLDOC_TYPE_PREFIXES.questionGouv}/${date}/${nb}`;

        return {
            score: await getPoldocTopicScore(id, topic)
        };
	},

    "validate-topic": async ({request, params: { date, nb }, locals: { mainDb }}) => {
        const data = await request.formData();

        const id = `${POLDOC_TYPE_PREFIXES.questionGouv}/${date}/${nb}`;

        const topic: string = data.get("topic") as string;
        if (!topic) {
            return fail(400, { annotation: { error: "missing topic"  }});
        }

        const row = mainDb.connection.prepare(`
            SELECT source_job FROM docs_labels
            WHERE doc_id=? AND label_name=?
        `).get(id, topic) as {
            source_job: string
        } || undefined;

        if (!row) {
            return fail(400, { annotation: { error: "could not find annotation" }})
        }

        const { source_job } = row;

        const result = mainDb.connection.prepare(`
            UPDATE docs_labels
            SET source_job = null
            WHERE doc_id=? AND label_name=?
        `).run(id, topic);

        if (result.changes != 1) {
            console.log(
                `validating topic ${topic} for document ${id}:`
                +` expected 1 row changed, got ${result.changes}`
            );
            return fail(500, { annotation: {error: "error validating topic" }});
        }

        console.log(`validated topic ${topic} for ${id}`);

        return { annotation: { validated: {
            topic,
            source_job,
        }}}
    },

    "cancel-validation":  async ({request, params: { date, nb }, locals: { mainDb }}) => {
        const data = await request.formData();

        const id = `${POLDOC_TYPE_PREFIXES.questionGouv}/${date}/${nb}`;

        const topic: string = data.get("topic") as string;
        if (!topic) {
            return fail(400, { annotation: { validation_cancelling: { error: "missing topic"  }}});
        }

        const source_job: string = data.get("source_job") as string;
        if (!source_job) {
            return fail(400, { annotation: { validation_cancelling: { error: "missing source_job"  }}});
        }

        const result = mainDb.connection.prepare(`
            UPDATE docs_labels
            SET source_job = ?
            WHERE doc_id = ? AND label_name = ?
        `).run(source_job, id, topic);

        if (result.changes != 1) {
            console.log(
                `cancelling validation of topic ${topic} for document ${id} (source job: ${source_job}):`
                +` expected 1 row changed, got ${result.changes}`
            );
            return fail(500, { annotation: { validation_cancelling: {error: "error validating topic" }}});
        }

        console.log(`cancelled validation of ${topic} for doc ${id} (source job: ${source_job})`)

        return { annotation: { validation_cancelling: { success: true }}};
    },

    "remove-topic": async ({request, params: { date, nb }, locals: { mainDb }}) => {
        const data = await request.formData();

        const id = `${POLDOC_TYPE_PREFIXES.questionGouv}/${date}/${nb}`;

        const topic: string = data.get("topic") as string;
        if (!topic) {
            return fail(400, { annotation: { remove: { error: "missing topic"  }}});
        }

        const result = mainDb.connection.prepare(`
            DELETE FROM docs_labels
            WHERE doc_id=? AND label_name=?
        `).run(id, topic);

        if (result.changes != 1) {
            throw new Error(`expected 1 row affected, got ${result.changes}`);
        }

        console.log(`removed topic ${topic} for doc ${id}`)

        return;
    },
} satisfies Actions;


async function getSuggestions(id: string, tfidfOnly: boolean) {
    let url = `http://annotation_suggestions:8027/suggestions/${id}`;
    if (tfidfOnly) {
        url += "?tfidf-only"
    }

    try {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error(`getting suggestions for ${id}: `+error);
        return null;
    }
}

async function getPoldocTopicScore(id: string, topic: string) {
    const response = await fetch(`http://annotation_suggestions:8027/tfidf/poldoc-topic-score/${id}?topic=${topic}`);
    const data = await response.json();
    return data as Suggestion;
}