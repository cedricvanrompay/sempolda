import { readdir } from 'node:fs/promises';
import type { PageServerLoad } from './$types';
import { JOBS_LOGS_DIRECTORY } from '.';


export const load: PageServerLoad = async () => {
    const jobs = Array<string>();

    for (const entry of await readdir(JOBS_LOGS_DIRECTORY)) {
        if (!entry.endsWith(".json")) {
            console.warn(`unexpected auto-annotation job log not ending in ".json": ${entry}`);
            continue;
        }

        jobs.push(entry.slice(0, -".json".length))
    }

    return {
        jobs,
    }
}