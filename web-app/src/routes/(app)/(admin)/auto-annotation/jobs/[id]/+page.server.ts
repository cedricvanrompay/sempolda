import path from "node:path";
import fs from 'node:fs';
import readline from "node:readline";
import type { PageServerLoad } from './$types';
import { error } from "@sveltejs/kit";

import { JOBS_LOGS_DIRECTORY } from "../..";

const ID_REGEXP = new RegExp("^[0-9a-zT-]+$");

export const load = (async ({ params: { id } }) => {
    if (!id.match(ID_REGEXP)) {
        error(400, "invalid job ID");
    }

    let start_time: string = "";
    let suffix: string = "";
    let poldocs_annotation_logs = Array();

    // copied from https://nodejs.org/api/readline.html#readline_example_read_file_stream_line_by_line

    const fileStream = fs.createReadStream(
        // no risk of path traversal
        // (see https://cwe.mitre.org/data/definitions/22.html)
        // because of the strict validation of the ID
        // against a limited character set
        path.join(JOBS_LOGS_DIRECTORY, id+".json"),
    );

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity,
    });

    let lineNb: number = 0;
    for await (const line of rl) {
        lineNb++;

        const data = await JSON.parse(line);

        if (lineNb == 1) {
            if (data.kind != "start") {
                throw Error(`expected kind to be "start" in first log line, got "${data.kind}"`);
            }

            start_time = data.start_time;
            if (!start_time) {
                throw Error(`missing start time in first log line`);
            }

            suffix = data.random_id_suffix;
            if (!suffix) {
                throw Error("missing random suffix in first log line");
            }
        } else {
            if (data.kind != "poldoc") {
                throw Error(`expected kind to be "poldoc" in first log line, got "${data.kind}"`)
            }

            poldocs_annotation_logs.push(data);
        }

    }

    if (!start_time || !suffix) {
        throw Error("missing start time or random suffix")
    }

    return {
        start_time,
        suffix,
        poldocs_annotation_logs,
    };
}) satisfies PageServerLoad;