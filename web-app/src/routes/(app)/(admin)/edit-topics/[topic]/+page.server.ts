import { fail, redirect } from '@sveltejs/kit';
import type { Actions, PageServerLoad } from './$types';
import type Database from 'better-sqlite3';

export const load: PageServerLoad = async ({ params, locals: { mainDb } }) => {
    const topic = mainDb.getTopic(params.topic);

    const forcedWeights = mainDb.connection
        .prepare(`
            SELECT term, weight
            FROM forced_term_weights
            WHERE label_name = ?
        `)
        .all(topic.name) as { term: string, weight: number }[];

    return {
        topic,
        forcedWeights,
        tfidfTerms: await getTopicTerms(topic.name),
    }
}

export const actions: Actions = {
    "change-name": async ({ request, params: { topic }, locals: { mainDb } }) => {
        const data = await request.formData();

        const newName = data.get("name") as string | null;

        if (!newName) {
            return fail(400, { "change-name": { error: "nom manquant" }});
        }

        if (mainDb.getSynonym(newName)) {
            return fail(400, { "change-name": { error: "existe déjà comme synonyme" }})
        }

        const result = mainDb.connection.prepare(`
            UPDATE labels
            SET name = ?
            WHERE name = ?
        `).run(newName, topic);

        // XXX the number of change may be variables due to cascading updates
        if (result.changes != 1) {
            throw Error(`expected 1 row changed, got ${result.changes}`);
        }

        // removing the "encodeURI" will lead to malformed URIs
        // when newName has accents
        redirect(303, encodeURI(`./${newName}`));
    },

    forcedWeight: async ({ request, params, locals: { mainDb } }) => {

        const data = await request.formData();

        const term: string = data.get('term') as string;
        if (!term) {
            return fail(400, { term, error: 'no term'});
        }

        if (!data.get('weight')) {
            return fail(400, { term, error: 'no weight' });
        }
        const weight: number = Number(data.get('weight'));

        const { topic } = params;

        if (weight == 0) {
            // TODO delete forced weights with a button instead,
            // so probably an API endpoint and not a form
            const result = mainDb.connection
                .prepare(`
                    DELETE FROM forced_term_weights
                    WHERE term=? AND label_name=?
                `)
                .run(term, topic);
    
            if (result.changes != 1) {
                return fail(400, { term, weight, error: 'term not found' });
            }
        } else {
            const row = mainDb.connection
                .prepare(`
                    SELECT weight FROM forced_term_weights
                    WHERE term=? and label_name=?
                `)
                .get(term, topic);
    
            let result: Database.RunResult;
    
            if (!row) {
                result = mainDb.connection
                    .prepare(`
                        INSERT INTO forced_term_weights (term, label_name, weight)
                        VALUES (?, ?, ?)
                    `)
                    .run(term, topic, weight);
            } else {
                result = mainDb.connection
                    .prepare(`
                        UPDATE forced_term_weights
                        SET weight=?
                        WHERE term=? and label_name=?
                    `)
                    .run(weight, term, topic);
            }
    
            if (result.changes != 1) {
                throw new Error(`${result.changes} rows affected`);
            }
        }
    },

    "remove-parent": async ({ request, params: { topic }, locals: { mainDb } }) => {
        const data = await request.formData();

        const parent: string = data.get('parent') as string;
        if (!parent) {
            return fail(400, { remove_parent: { error: "missing parent" } });
        }

        const info = mainDb.connection.prepare(`
            DELETE FROM labels_parent_labels
            WHERE child_label_name=? AND parent_label_name=?
        `).run(topic, parent);

        if (info.changes != 1) {
            throw new Error(`expected 1 row changed, got ${info.changes}`);
        }
    
        return { remove_parent: { success: true }};
    },

    "add-parent": async ({ request, params: { topic }, locals: { mainDb } }) => {
        const data = await request.formData();

        const parent: string = data.get('topic') as string;
        if (!parent) {
            return fail(400, { add_parent: { error: "missing topic form field" } });
        }

        mainDb.connection.prepare(`
            INSERT INTO labels_parent_labels
                (child_label_name, parent_label_name)
            VALUES (?, ?)
        `).run(topic, parent);

        return { add_parent: { success: true }};
    },
} satisfies Actions;

async function getTopicTerms(topic: string) {
    const response = await fetch(`http://annotation_suggestions:8027/tfidf/topic-terms/${topic}`);
    const data = await response.json();
    return data as [number, string][];
}