import type { LayoutServerLoad } from './$types';

export const load = (async ({ locals: { mainDb }}) => {
    return {
        persons: mainDb.getAllPersons(),
    };
}) satisfies LayoutServerLoad;