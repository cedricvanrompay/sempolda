import type { PageServerLoad } from './$types';
import { NoResult } from '$lib/database';
import { error as svelteError } from '@sveltejs/kit';
import type { Poldoc } from '$lib/types';

import { getInterventions } from '$lib/questionsGouv'

export const load: PageServerLoad = async ({ params, locals: { mainDb } }) => {
    const { date, nb } = params;

    const id = `assemblee-nationale/questions-gouv/${date}/${nb}`;

    let poldoc: Poldoc;
    try {
        poldoc = mainDb.getPoldoc(id);
    } catch (err) {
        if (err instanceof NoResult) {
            throw svelteError(404, `document introuvable`);
        }

        throw err;
    }

    return {
        poldoc,
        interventions: getInterventions(id),
    }
}