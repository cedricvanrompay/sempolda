import { error as SveltekitError, redirect } from '@sveltejs/kit';

import { MainDb, NoResult } from '$lib/database';
import type { Poldoc, Topic } from '$lib/types';
import type { PageServerLoad } from './$types';
import { sortPoldocList } from '$lib/utils';

function* walkTopicTree(root: string, mainDb: MainDb): Generator<string, void, undefined> {
    yield root;

    const subtopics = mainDb.getSubtopicsNames(root);

    for (const subtopic of subtopics) {
        for (const yielded of walkTopicTree(subtopic, mainDb)) {
            yield yielded;
        }
    }
}

export const load: PageServerLoad = async ({ url, locals: { mainDb } }) => {
    const topicParam = url.searchParams.get("topic");
    const personID = url.searchParams.get("person_id");
    const sources = url.searchParams.getAll("source");

    if (sources.length > 1) {
        SveltekitError(400, "pas encore implémenté: multi-sources")
    }

    if (personID && topicParam) {
        SveltekitError(400, "recherche combinée pas encore supportée");
    }

    if (personID) {
        const person = mainDb.getPerson(personID);

        if (!person) {
            SveltekitError(404, `personne non trouvée`)
        }

        const poldocs = mainDb.getPoldocsByPerson(person.id);

        return {
            search: {
                person,
                results: poldocs,
            }
        }
    }


    if (!topicParam) {
        return {
            latestDocs: mainDb.listLatestPoldocs(),
        };
    }

    const synonym = mainDb.getSynonym(topicParam);

    if (synonym) {
        const u = new URL(url.href);
        u.searchParams.set("topic", synonym);
        redirect(308 /* permanent redirect */, u);
    }

    let topic: Topic;
    try {
        topic = mainDb.getTopic(topicParam);
    } catch (err) {
        if (err instanceof NoResult) {
            throw SveltekitError(400,
                `Le sujet « ${topicParam} » n'existe pas`,
            );
        }

        throw err;
    }

    const processedPoldocIds = new Set<string>();
    const poldocs = new Array<Poldoc>();

    for await (const t of walkTopicTree(topic.name, mainDb)) {
        for (const id of mainDb.getPoldocsIdsByLabel(t)) {
            if (!processedPoldocIds.has(id)) {
                processedPoldocIds.add(id);
                poldocs.push(mainDb.getPoldoc(id));
            }
        }
    }

    sortPoldocList(poldocs);

    return {
        search: {
            topic,
            results: poldocs,
        }
    };
}