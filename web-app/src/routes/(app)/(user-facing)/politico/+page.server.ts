import type { PageServerLoad } from './$types';

export const load = (async ({ locals: { mainDb } }) => {
    return {
        poldocs: mainDb.connection.prepare(`
        SELECT id, title FROM docs
        WHERE id LIKE 'politico/%'
    `).all() as {
            id: string,
            title: string,
        }[]
    };
}) satisfies PageServerLoad;