import type { LayoutServerLoad } from './$types';

export const load: LayoutServerLoad = async ({ locals: { account }}) => {
	const result: {
		username?: string,
	} = {};

	if (account) {
		result.username = account.username;
	}

	return result;
};