import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ locals: { mainDb }}) => {
    const row = mainDb.connection
        .prepare( "SELECT COUNT(*) AS count FROM (SELECT DISTINCT doc_id FROM docs_labels);")
        .get() as { count: number };

    return {
        nbAnnotatedDocs: row['count'],
        latestDocs: mainDb.listLatestPoldocs(),
    };
};
