import type { RequestHandler } from './$types';
import { json } from '@sveltejs/kit';

export const GET: RequestHandler = async (
    {
        url,
        locals: { autocompleters: { persons: personAutocompleter } }, 
    }
) => {
    const query = url.searchParams.get('q') || '';

    return json(
        personAutocompleter.complete(query)
    );
};