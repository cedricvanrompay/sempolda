import type { RequestHandler } from './$types';
import { json } from '@sveltejs/kit';

export const GET: RequestHandler = async (
    {
        url,
        locals: { autocompleters: { topics: topicsAutocompleter } }, 
    }
) => {    const query = url.searchParams.get('q') || '';

    return json(topicsAutocompleter.complete(query));
};