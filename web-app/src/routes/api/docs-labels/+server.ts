import { NoResult } from '$lib/database';
import { error as svelteError } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request, locals: { mainDb } }) =>  {
    const {docId, labelName} = await request.json();

    try {
        mainDb.getTopic(labelName);
    } catch(error) {
        if (error instanceof NoResult) {
            throw svelteError(409, "topic does not exist");
        }

        throw error;
    }

    mainDb.connection
        .prepare(`
            INSERT INTO docs_labels (doc_id, label_name)
            VALUES (?, ?)
        `).run(docId, labelName);

    return new Response('', {status: 201})
}
