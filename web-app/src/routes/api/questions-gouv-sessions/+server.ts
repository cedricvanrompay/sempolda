import type { RequestHandler } from './$types';
import { json } from '@sveltejs/kit';
import type { Session } from './types';
import { getMetadata, type Metadata } from '$lib/metadata';
import { POLDOC_TYPE_PREFIXES } from '$lib/types';


export const GET: RequestHandler = async ({ locals: { mainDb }}) =>  {
    const rows = mainDb.connection.prepare(`
        SELECT
            id,
            title,
            SUM(CASE WHEN doc_id IS NULL THEN 0 ELSE 1 END) as nbLabels
        FROM docs d LEFT JOIN docs_labels l ON d.id = l.doc_id
        GROUP BY id
    `).all() as any[];

    const bySessionDate = new Map<string, Session>();
    for (const row of rows) {
        const { id, title, nbLabels } = row;

        const date = id.split("/")[2];

        if (!bySessionDate.has(date)) {
            bySessionDate.set(date, {
                has_unannotated: false,
                questions: [],
            });
        }

        const annotated = nbLabels > 0;

        const session = bySessionDate.get(date) as Session;
        session.questions.push({ id, title, annotated });

        if (!annotated) {
            session.has_unannotated = true;
        }

    }

    const result: {
        [date: string]: Session
    } = Object.fromEntries(bySessionDate)

    for (const date of Object.keys(result)) {
        let metadata: Metadata;

        try {
            metadata = getMetadata(`${POLDOC_TYPE_PREFIXES.questionGouv}/${date}/01`)
        } catch (error: any) {
            if (error.code === "ENOENT") {
                // file not found
                continue;
            }

            throw error;
        }
        
        if (metadata.scrapped_time) {
            result[date].scrapped_time = metadata.scrapped_time
        }
    }

    return json(result);
}