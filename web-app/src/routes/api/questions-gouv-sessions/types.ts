import type { Poldoc } from "$lib/types"

type PoldocWithAnnotations = Poldoc & {
    annotated: boolean
}

export type Session = {
    has_unannotated: boolean
    questions: PoldocWithAnnotations[]
    scrapped_time?: string
}