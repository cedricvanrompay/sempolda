import type { RequestHandler } from './$types';
import { error as svelteError, json } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ url, locals: { mainDb } }) => {
    const name = url.searchParams.get('name');

    if (!name) {
        throw svelteError(400, 'bad request: param "name" is required');
    }

    const s = mainDb.getSynonym(name);

    if (s) {
        return json([s]);
    } else {
        return json([]);
    }
}