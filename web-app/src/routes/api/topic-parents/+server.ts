import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request, locals: { mainDb } }) => {
    const { child, parent } = await request.json();

    mainDb.connection.prepare(`
        INSERT INTO labels_parent_labels
            (child_label_name, parent_label_name)
        VALUES (?, ?)
    `).run(child, parent);

    return new Response('', {status: 201});
};
