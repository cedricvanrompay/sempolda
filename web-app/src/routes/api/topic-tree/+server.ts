import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

const collator = new Intl.Collator('fr');

export const GET: RequestHandler = async ({ url, locals: { mainDb } }) =>  {
    const root = url.searchParams.get('root');
    const names = (
        root ? mainDb.getChildrenNames(root) : mainDb.getRootLabelsNames()
    );

    names.sort(collator.compare);

    return json(names.map((name: string) => ({
        name,
        children: mainDb.getChildrenNames(name),
    })));
}