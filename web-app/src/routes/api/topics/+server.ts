import { NoResult } from '$lib/database';
import type { Topic } from '$lib/types';
import { error as svelteError, json, error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ url, locals: { mainDb } }) => {
    const name = url.searchParams.get('name');

    if (!name) {
        throw svelteError(400, 'bad request: param "name" is required');
    }

    try {
        const topic = mainDb.getTopic(name);
        return json([topic])
    } catch (error) {
        if (error instanceof NoResult) {
            return json([]);
        }

        throw error;
    }
};

export const POST: RequestHandler = async ({
    request,
    locals: { mainDb, autocompleters: { topics: topicsAutocompleter } },
}) => {
    const topic = await request.json() as Topic;

    if (mainDb.getSynonym(topic.name)) {
        error(409 /* conflict */, `"${topic.name}" existe déjà comme synonyme`);
    }

    mainDb.connection.transaction(() => {
        const info = mainDb.connection.prepare("INSERT INTO labels (name, description) VALUES (?, ?)")
            .run(topic.name, topic.description);

        if (info.changes != 1) {
            throw new Error("unexpected number of rows changed")
        }

        for (const parent of topic.parents) {
            mainDb.connection.prepare(`
                INSERT INTO labels_parent_labels
                    (child_label_name, parent_label_name)
                    VALUES (?, ?)
            `).run(topic.name, parent);
        }
    // note that "database.transaction" returns a *function*
    // that must then be *invoked*, otherwise nothing happens
    })();

    topicsAutocompleter.add(topic.name, topic.name);

    return new Response('', {status: 201});
}