import type { RequestHandler } from './$types';

export const PUT: RequestHandler = async ({ params, request, locals: { mainDb } }) => {
    const { name } = params;

    const description = await request.json();

    mainDb.connection.prepare(`
        UPDATE labels
        SET description=?
        WHERE name=?
    `).run(description, name);

    return new Response();
};