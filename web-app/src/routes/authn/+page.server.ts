import type { PageServerLoad } from './$types';
import { error as svelteError } from '@sveltejs/kit';
import { SystemDb, Unauthenticated } from '$lib/authentication';

export const load: PageServerLoad = async ({ url, cookies }) => {
    const systemDb = new SystemDb()
    
    const token = url.searchParams.get("token");

    try {
        const username = systemDb.createCookieFromToken(token, cookies);
        return {
            username
        }
    } catch(error) {
        if (error instanceof Unauthenticated) {
            throw svelteError(403);
        }

        throw error;
    }
}